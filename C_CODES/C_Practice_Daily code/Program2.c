/*
 1
 3 2 1
 5 4 3 2 1
 3 2 1
 1
 */
#include<stdio.h>

void main(){

	int row;

	printf("Enter rows : \n");
	scanf("%d" ,&row);
	 int x=1;
	 for( int i=0; i<=row; i++){

		 int num = i*2-1;

		 for( int j=1; j<=x; j++){

			 printf("%d\t" , num);
			 num--;
		 }
		 printf("\n");

		 if( i< ( row/2+1)){

			 x=x+2;

		 }
		 else{
			 x =x-2;
		 }
	 }
}


