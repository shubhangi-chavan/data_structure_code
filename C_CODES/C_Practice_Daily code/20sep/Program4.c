/*
 D C B A 
 c b a
 B A
 a */
 
#include<stdio.h>

void main(){

	int row;
	printf("Enter row: \n");
	scanf("%d" ,&row);
	
	for( int i=1; i<=row; i++){

		int ch1 = row+65-i;

		for( int j=row; j>=i; j--){

			if( i %2 != 0) {

				printf("%c\t" , ch1);
				ch1--;

			}

			else{

				printf("%c\t", ch1+32);
				ch1--;

			}
		}
		printf("\n");
	}
}

