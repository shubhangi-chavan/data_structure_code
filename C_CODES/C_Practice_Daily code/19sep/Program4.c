
/*
 5 4 3 2 1
 D C B A
 3 2 1
 B A
 1 */
 
#include<stdio.h>

void main() {

	int row;
	printf("Enter row number : \n");
	scanf("%d",&row);

	for( int i=1; i<=row; i++){

		int ch = row+65-i;
		int x =row-i+1;

		for( int j = row; j>=i; j--){

			if( i % 2 != 0){
				printf( " %d ", x );
				x--;

		}
			else{
				printf(" %c ",ch);
				ch--;
			}

		}
		

		printf("\n");

	}
}

