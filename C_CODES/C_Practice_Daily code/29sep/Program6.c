/*
 WAP to print maximum digit in number*/

#include<stdio.h>

void main(){

	int num;
	printf("Enter number : \n");
	scanf("%d" ,&num);

	int max=0,rem;

	while( num>0){

		rem = num%10;

		if( max < rem){
			max=rem;
		}
		num =num/10;
	}
	printf("Maximum number is %d" ,max);
}

