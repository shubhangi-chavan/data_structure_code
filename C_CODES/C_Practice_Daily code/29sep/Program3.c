/*
         1
       2 4
   3   6 9 
 4 8  12 16
   5  10 15
      6	 13
	 7
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter row :\n");
	scanf("%d" ,&row);

	int x=1;

	for( int i=1; i<=row; i++){

		int num=1;

		for( int k =1; k<=(row-3)-num; k++){

			printf("\t");

		}
		for( int j=1; j<=x; j++){
			printf("%d\t" , i*j);
			
		}
		printf("\n");

		if(i<(row/2)+1){
			x++;
		}
		else{
			x--;
		}
	}
}

