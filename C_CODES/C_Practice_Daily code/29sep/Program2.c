/*
         1
       3 1
    5  3 1 
       3 1
	 1
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter row :\n");
	scanf("%d" ,&row);

	int x=1;

	for( int i=1; i<=row; i++){

		int num=1;

		for( int k =1; k<=(row-2)-1; k++){

			printf("\t");

		}
		for( int j=1; j<=x; j++){
			printf("%d" , num);
			num=num+2;
		}
		printf("\n");

		if(i<(row/2)+1){
			x++;
		}
		else{
			x--;
		}
	}
}

