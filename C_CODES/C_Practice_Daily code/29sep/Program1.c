/*
 
   0 0 0 0 0
     1 2 3 4
       4 6 8
         9 12
	   16

	   */

#include<stdio.h>

void main(){

	int row;
	printf("Enter row : \n");
	scanf("%d" ,&row);

	for( int i=0; i<row; i++){

		int num=i;
		for( int j =1; j<=i; j++){


			printf("\t");

		}
		for( int k=1; k<row-i+1; k++){

			printf("%d\t" , num*i);
			num++;
		}
		printf("\n");
	}
}

