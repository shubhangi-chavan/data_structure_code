/* WAP to print Niven number */

#include<stdio.h>

void main(){

	int start,end;
	printf(" Enter start : \n");
	scanf("%d" , &start);
	printf("Enter end : \n");
	scanf("%d", &end);

	for( int i=start; i<=end; i++){

		int sum=0,rem , temp;
		temp = i;
		while( temp != 0){

			rem = temp % 10;
			sum = sum+rem;
			temp = temp/10;
		}

		if( i % sum ==0 ){

			printf("%d " , i);

		}
	}
}


