/*
 A B C D
   B C D
     C D
       D
 */

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows: \n");
	scanf("%d" , &row);

	for( int i =1; i<=row; i++){

		int ch = i +64;
		for(int j=1; j<=i; j++){

			printf("\t");
		}
		for( int k =i; k<=row; k++){
			printf("%c\t" , ch);
			ch++;
		}
		printf("\n");
	}
}

