/*
         d
      C  d
    b C  d
  A b C  d
 */

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows: \n");
	scanf("%d" , &row);

	for( int i =1; i<=row; i++){

		int ch = i +98;
		for(int j=row-1; j>=i; j--){

			printf("\t");
		}
		for( int k =1; k<=i; k++){
			if( k % 2 == 0){

			printf("%c\t" , ch);
			ch = ch-2;
		}

			else{
			   printf("%c\t" , ch-29);
			   ch = ( ch-29)-2;

			}
		}
	

		printf("\n");
	}
}

