/* WAP tp print the sum of digits in given number */

#include<stdio.h>

void main(){
	
	int num, sum=0, rem=0;

	printf("Enter number : \n");
	scanf("%d", &num);

	while( num!=0){

		rem = num % 10;
		num = num/10;

		sum = sum+rem;
	}
	printf("%d" , sum );

}


