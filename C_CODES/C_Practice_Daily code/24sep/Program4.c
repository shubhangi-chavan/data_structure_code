/*
 1
 1  2
 1  2  3
 1  2  3  4 
 1  2  3
 1  2
 1 
  */

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows : \n");
	scanf("%d" , &row);
	int x =1;

	for( int i =1; i<=row; i++){

		int y =1;

		for( int j =1; j<=x; j++){
			printf("%d\t" , y);
			y++;

		}
		printf("\n");
	
	if( i < ( row / 2 +1)){
		x++;

	}
	else{
		x--;

	}
	}
}

