/*
                 1
	      1	 2  3
	   1  2  3  4  5
	1  2  3  4  5  6  7

	*/
#include<stdio.h>

void main(){
	int row;
	printf("Enter row :\n");
	scanf("%d" , &row);

	
	for( int i=1; i<=row; i++){

		int num=1;

		for( int j=1; j<=(i*i)-1; j++){

			printf(" ");
		}
		for( int k =1; k<=(i*2)-1; k++){

			printf("%d " , num);
		        num++;
		}
	         printf("\n");
	}
}

