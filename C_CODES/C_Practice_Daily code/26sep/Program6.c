/* WAP to print sum of even digits in given number*/

#include<stdio.h>

void main(){

	int num;
	printf("Enter number : \n");
	scanf("%d" , &num);
	 int sum=0,rem=0;

	 while( num !=0){

		 rem = num%10;
		    if( rem % 2 == 0){
			    sum =sum + rem;
		    }
		    num=num/10;
	 }
	 printf("%d" ,sum);
}

