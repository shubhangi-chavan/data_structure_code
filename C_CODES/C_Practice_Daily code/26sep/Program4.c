/*
                   1
               3 2 1
           5 4 3 2 1
       7 6 5 4 3 2 1
 9  8  7 6 5 4 3 2 1

 */

#include<stdio.h>

void main(){

	int row;
	printf("Enter row : \n");
	scanf("%d" , &row);
	

	for( int i =1; i<=row; i++){
	          int x =1;	

		 for( int j=1; j>=(row-i)*2; j--){
			 printf(" ");
		 }
		 for( int k =1; k<=(i*i)-1; k++){

			 printf("%d\t" , x);
			 x++; 

		 }
		 printf("\n");
	}
}

