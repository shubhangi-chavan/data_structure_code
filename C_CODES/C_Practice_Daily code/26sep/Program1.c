/*
 A  b  C  d
    d  E  f
       F  g
          g

	  */

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows: \n");
	scanf("%d", & row);

	int ch= 61+row;
	for( int i =1; i<=row; i++){

		for( int j=2; j<=i; j++){

			printf("\t");
		}
		for( int k=1; k<=row-i+1; k++){
			if((i+k) % 2 ==0 ){
				printf("%c\t" , ch);
				ch++;
			}
			else{
				printf("%c\t" , ch+32);
				ch++;
			}
		}
		printf("\n");
		ch--;
	}
}


