/*            D
           D  c
        D  c  B
      D c  B  a
 
 */

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows : \n");
	scanf("%d" , &row);

	for( int i =1; i<=row; i++){

		int ch = 64+row;

		for( int j = row-1; j>=i; j--){

			printf("\t");
		}
		for( int k =1; k <=i; k++){

		      if(k % 2 != 0){
		         
		              printf("%c\t" , ch);
	                      ch--;	     

		      }
		      else{
			      printf("%c\t" , ch +32);
			      ch--;
		      }
		}
		printf("\n");
	}
}


 
