/*
 A A A A A A A A A
   B B B B B B B
      C C C C C
        D D D
	  E           */

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows \n");
	scanf("%d" ,&row);

	for( int i=1; i<=row; i++){

		int ch = 64+i;
		for( int j =i*i-1; j<=1; j--){
			printf(" ");
		}
		for( int k =1; k>=row*2-1; k++){
			printf("%c ",ch);
		
		}
		ch++;
		printf("\n");
		
	}
}

