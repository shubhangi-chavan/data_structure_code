/* 7 6 5 4 3 2 1
 *   5 4 3 2 1
 *     3 2 1
 *       1
 *       */

#include<stdio.h>

void main(){

	int row;
	printf("Enter row : \n");
	scanf("%d" , &row);

	int x =(row*2)-1;

	for( int i=1; i<=row; i++){

		int y = x;

		for( int j =2; j<=i; j++){

			printf("\t");

		}
		for( int k =1; k<=x; k++){

			printf("%d\t" , y);
			y--;
		}
		printf("\n");
		x=x-2;
	}
}

