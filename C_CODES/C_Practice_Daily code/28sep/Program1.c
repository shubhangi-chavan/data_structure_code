/*
 A A A A A A A A A
   B B B B B B B
     C C C C C
       D D D
         A
         
 */

#include<stdio.h>

void main(){

	int row;
	printf("Enter row :\n");
	scanf("%d" ,&row);

	int x = (row*2)-1;

	for( int i=1; i<=row; i++){

	        int ch = 64+i;
		
		for( int j=2; j<=i; j++){

			printf("\t");
		}
		for(int k = 1; k<=x; k++){

			printf("%c\t" , ch);
			
		}
		printf("\n");
		x=x-2;
	}
}


