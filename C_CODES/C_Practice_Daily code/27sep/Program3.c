/*
            1
	  3 2 1
        5 4 3 2 1
     7	6 5 4 3 2 1
     */

#include<stdio.h>

void main(){

	int row;
	printf("Enter row : \n");
	scanf("%d" ,&row);
	for( int i=1; i<=row; i++){

		int num = 1;
                     
		for( int j=1; j<=(i*i)-1; j++){

			printf(" ");
		}
		for( int k=1; k<=(i*2)-1; k++){

			printf("%d " ,num);
			num=num+2;
		}
		printf("\n");
	}
}


