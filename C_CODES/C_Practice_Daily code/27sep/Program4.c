/*
 9 8 7 6 5 4 3 2 1
     7 6 5 4 3 2 1
       6 5 4 3 2 1
             3 2 1
	         1
		 */
#include<stdio.h>

void main(){
	int row;
	printf("Enter row:\n");
	scanf("%d" ,&row);
	
	for( int i=1; i<=row; i++){

		int num=row*2-i;

		for( int j = (row-1)*2; j>=1; j--){

			printf("\t");

		}
		for( int k=(i*i)-1; k>=1; k--){
			printf("%d\t" , num);
			num--;
		}
		printf("\n");
	}
}

