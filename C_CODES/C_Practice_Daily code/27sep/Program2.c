/*
           D
	d  E
     D	c  B
  d  E  f  G
  */

#include<stdio.h>

void main(){

	int row;
	printf("Enter row : \n");
	scanf("%d" , &row);
	
	for( int i=1; i<=row; i++){
                 
		int ch = 67 + i;
		
		for( int j = row-1; j>=i; j--){
			printf("\t");

		}
		for( int k =1; k<=i; k++){

			if( ((i+k)) %2 != 0){
				printf("%c\t",ch+32);
				ch++;
			}
			else{
				printf("%c\t" , ch);
				ch--;
			}
		}
		printf("\n");
	}
}



