/*
 A b C d E 
   e D c B
     B c D
       d C
         C
	 */

#include<stdio.h>

void main(){

	int row;
	printf("Enter row : \n");
	scanf("%d" , &row);
	
         

	for( int i=1; i<=row; i++){
               int ch = 64+i;    
               
		for( int j =1; j<=i; j++){

			printf("\t");

		}
		for( int k=1; k<=row-i+1; k++){

			if(( i+k)%2 ==0){

				printf("%c\t" , ch);
				ch++;
			}
			else{
				printf("%c\t", ch+32);
				ch++;
				
                        
			}
		}
		printf("\n");
	}
}

