#include<stdio.h>
 struct college {
	  char cName[20];
	  char type[20];
          int seats;
	  double fees;
 } ENG = {"COEP","GOV",570,26.000};
void main(){
	struct college Med={"BJ","GOV",200,10000.04};
	struct college *eptr = &ENG;
	struct college *Mptr= &Med;

	printf("%s\n",eptr->cName);
	printf("%s\n",eptr->type);
	printf("%d\n",eptr->seats);
	printf("%f\n",eptr->fees);
	printf("%p\n",eptr);
	printf("%s\n",Mptr->cName);
	printf("%s\n",Mptr->type);
	printf("%d\n",Mptr->seats);
	printf("%.2f\n",Mptr->fees);
	printf("%p\n",Mptr);
	printf("%ld\n",sizeof(struct college));
	printf("%ld\n",sizeof*(eptr));
	printf("%ld\n",sizeof*(Mptr));
	}

