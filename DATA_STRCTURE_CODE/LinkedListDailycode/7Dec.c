
#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	struct Node *prev;
	int data;
	struct Node *next;
}Node;

Node *head=NULL;

Node* createNode(){

	Node *newNode=(Node*)malloc(sizeof(Node));

	Node *prev =NULL;

	printf("Enter data\n");
	scanf("%d",&newNode->data);
	
	Node *next = NULL;
	newNode->next=NULL;

	return newNode;
}

void addNode(){

	Node *newNode = createNode();

	if( head == NULL){

		head=newNode;
	}
	else{
		Node *temp=head;
		while(temp->next != NULL){

			temp=temp->next;
		}
		temp->next=newNode;
		newNode->prev=temp;
	}
}

void addFirst(){

             Node *newNode=createNode();

	     if( head==NULL){

		     head=newNode;
	     }
	     else{
		     newNode->next=head;
		     head->prev=newNode;
		     head=newNode;
	     }
}

int countNode(){

	Node *temp=head;
	int count=0;
	while(temp!=NULL){
		count++;
		temp=temp->next;
	}
	printf("count: %d\n",count);
	return count;
}


int addAtpos( int pos){


	int count=countNode();

	if( pos <=0 || pos>=count+2){

		printf("Invalid node position\n");
		return -1;
	}
	else{

		if( pos == count+1){
			addNode();
			
		}
		else if(pos == 1){
			addFirst();
			
		}
		else{

			Node *newNode =createNode();
			Node *temp=head;
			while(pos-2){

				temp=temp->next;
				pos--;
			}
			newNode->next=temp->next;
			newNode->prev=temp;
		        temp->prev->next=newNode;
		        temp->next=newNode;
		}
	}
          return 0;
}

void printNode(){

	Node *temp =head;

	while( temp->next !=NULL){

		printf("|%d->",temp->data);
		temp=temp->next;
	}
	printf("|%d|",temp->data);
}

void deleteFirst(){
          
	int count=countNode();

	if( head ==NULL){
		printf("No node to delete\n");
	}
	else if(count==1){

		free(head);
		head=NULL;
	}
	else{

		head=head->next;
		free(head->prev);
		head->prev=NULL;
	}

}

int deleteLast(){

	int count = countNode();

	if( head == NULL){

		printf("Empty linked list\n");
		return -1;
	}
	else if( count == 1){

		free(head);
		head=NULL;
	}
	else{
		Node *temp = head;

		while(temp->next->next != NULL){
			temp=temp->next;
		}
		free(temp->next);
		temp->next=NULL;
	}
	return 0;


}

int deleteAtpos( int pos){

	int count =countNode();

	if( pos >= 0 || pos > count){

		printf("Invalid position\n");
		return -1;
	}
	else{

		if(pos == count){
			deleteLast();
		}
		else if(pos == 1){

			deleteFirst();
		}
		else{
			Node *temp=head;
			while(pos-2){

				temp=temp->next;
				pos--;
			}
			temp->next=temp->next->next;
			free(temp->next->prev);
			temp->next->prev=temp;
		
		}

		return 0;
	}
}

int revList(){

	if( head == NULL){
		printf("linkedlist empty\n");
		return -1;
	}

	else{

 
               Node *temp1 =head;
               Node *temp2 =head;

                 while( temp2 ->next != NULL){
	               temp2=temp2->next;
                 } 

                    int count=countNode();
                    int cnt =count/2;
                           while(cnt){
	                     int temp =temp2->data;
	                     temp2->data=temp1->data;
	                     temp1->data=temp;
                             
                            temp1=temp1->next;
                            temp2=temp2->prev;
                            cnt--;
			   }

			    

	} 
        return 0;
}



       


void main(){

	char choice;

	do{

		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addAtpos\n");
		printf("4.deleteFirst\n");
		printf("5.deleteLast\n");
		printf("6.deleteAtpos\n");
		printf("7.countNode\n");
		printf("8.printNode\n");
		printf("9.revList\n");
		printf("Enter choice\n");
		int ch;
		scanf("%d",&ch);


		switch(ch){


			case 1:

				addNode();
				break;

		        case 2:
	                       
	                       addFirst();
			       break;

			case 3:
                               {
                               int pos;
			       printf("Enter position: \n");
			       scanf("%d",&pos);
			       addAtpos(pos);
			       }
			       
			        break;

			case 4:
			        
			       deleteFirst();
			       break;

			case 5:

                               deleteLast();
                               break;
			case 6:
                               {
                               int pos;
                               printf("Enter position: \n");
                               scanf("%d",&pos);
                               deleteAtpos(pos);
                               }

			case 7:
			      
			       countNode();
			       break;

			case 8:
			     
			       printNode();
			       break;
			      
			case 9:
			       
			       revList();
			       break;       

			default:

			       printf("wrong choice\n");
			       break;
		}
	        getchar();
	        printf("Do you want to continue ?\n");
	        scanf("%c",&choice);
	}while( choice == 'Y' || choice == 'y');
}

			       
			        








	  

