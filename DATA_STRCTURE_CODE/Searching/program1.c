// find floor number using Binaray search

#include<stdio.h>

int floorNum( int arr[] , int size, int ele){

	int start =0 , end =size -1,store = -1;

	while( start <= end){


		int mid = (start + end) /2;

		if( arr[mid] == ele){

			return arr[mid];

		}

		if( arr[mid] > ele ){

			end = mid-1;

		}
		if( arr[mid] < ele){

	         	store = arr[mid];
			start = mid +1;

		}

	
	}
	return store;
}

void main(){

	int size;
	printf("Enter size of array\n");
	scanf("%d",&size);

	int arr[size];
	printf("Enter array elements\n");

	for( int i=0; i<size; i++){

		scanf("%d",&arr[i]);

	}

	int ele;
	printf("Enter element do you want to search\n");
	scanf("%d",&ele);

	int Floor = floorNum( arr ,size ,ele);

	if( Floor == -1){

		printf("Not having any floor number\n");
	}
	else{

		printf("%d is floor number \n",Floor);
	}
}

