// binary search

#include<stdio.h>

int binaraySearch( int arr[] , int size, int ele){

	int start = 0, end =size-1;

	while( start <= end){

		int mid = start + end/2;

		if( arr[mid] ==ele){

			return mid;

		}
		if( arr[mid] > ele){

			end =mid-1;
		}

		if( arr[mid] < ele){

			start = mid +1;

		}

	}

	return -1;

}

void main(){

	int size;
	printf("Enter size of array\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements\n");

	for( int i=0; i<size; i++){

		scanf("%d",&arr[i]);

	}

	int ele;
	printf("Enter element do you want to search\n");
	scanf("%d",&ele);

	int Ele = binaraySearch( arr , size , ele);

	if( Ele == -1){

		printf("Element not found\n");
	}
	else{

		printf("Element found at index : %d",Ele);
	}

}





