// String palindrome without recursion

#include<stdio.h>
#include<stdbool.h>

void isPalindrome( char arr[] , int size){


	int start =0;
	int end = size-1;
	int flag=0;

	while( start<end){

		if( arr[start++] == arr[end--]){

			flag = 1;


		}
		else{

			flag = 0;

		}
	}

	if( flag ==0){

		printf("String is not palindrome\n");
	}

	else{

		printf("Palindrome\n");
	}

}

void main(){

	char arr[] = {'M','A','D','A','M'};
         
	 isPalindrome( arr , 5);



}

         	


