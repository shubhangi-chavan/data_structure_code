#include<stdio.h>

int Fact( int x ){

	static int fact =1;

	fact = fact * x;

	if( x != 1){

		Fact(--x);
	}
	return fact;
}

void main(){

	int num = Fact(5);

	printf("Factorial of number is %d ",num);
}

