// Char array = search char in string = with recursion

#include<stdio.h>

int Search( char arr [] ,int size, char search){


		if( size == 0){
                   
			return -1;
		}

		if( search == arr[size-1]){

			return size -1;
		}

		return Search( arr , size-1, search);

}

void main(){
          

	char search = 'D';
	char arr[] = {'A','B','C','D','E'};

	int ret = Search( arr ,5,search);
	printf("%d",ret);

}

