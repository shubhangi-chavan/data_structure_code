// Reverse number
//

#include<stdio.h>

int numRev( int num){
         
	int sum =0, rem;

	while( num){

		rem = num % 10;

		sum =sum *10 +rem;

		num=num/10;

	}
	return sum;

}

void main(){


	int num;
	printf("Enter number\n");
	scanf("%d",&num);

	int sum = numRev( num);
	printf("%d",sum);

}





