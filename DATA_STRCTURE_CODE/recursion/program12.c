// Recursion in Array  = program 12 - without recursion
//


#include<stdio.h>


int sumOfnum( int arr[] , int size){


	int sum =0;

	for( int i=0; i<size; i++){

		sum = sum+arr[i];

	}
	return sum;

}

void main(){


	int size;
	
	printf("Enter size of array\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements\n");

	for( int i=0; i<size; i++){

		scanf("%d",&arr[i]);

	}

	int sum = sumOfnum( arr , size);
	printf(" Sum of elements is SUM = %d" ,sum);
}

