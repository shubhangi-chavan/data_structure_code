// Recursion in Array  = program 13 - with recursion



#include<stdio.h>


int sumOfnum( int arr[] , int size){

    if( size ==  0){

	    return 0;
    }

    return sumOfnum( arr , size-1 )+ arr[size-1];

}




void main(){


	int size;
	
	printf("Enter size of array\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements\n");

	for( int i=0; i<size; i++){

		scanf("%d",&arr[i]);

	}

	int sum = sumOfnum( arr , size);
	printf(" Sum of elements is SUM = %d" ,sum);
}

