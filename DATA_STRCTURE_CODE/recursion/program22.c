// Reverse number = with recursion


#include<stdio.h>

int numRev( int num , int sum ){

	if( num == 0){

		return sum;

	}

	int rem = num % 10;
        sum = sum * 10 + rem;
  return numRev(num / 10, sum);
}
         
	

void main(){


	int num;
	printf("Enter number\n");
	scanf("%d",&num);

	int sum = numRev( num,0);
	printf("%d",sum);

}





