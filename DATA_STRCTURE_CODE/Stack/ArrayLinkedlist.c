// stack using Linkedlist

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct Node{

	int data;
	struct Node *next;

}Node;


Node *head = NULL;
int flag=0;
int countNode=0;

int elecount(){

	Node *temp=head;
	int count=0;

	while( temp != NULL){

		count++;
		temp =temp->next;
	}
	return count;
}

bool isFull(){

	if( elecount() == countNode){

		return true;
	}

	else{

		return false;
	}
}



Node* createNode(){

	Node *newNode = (Node*)malloc(sizeof(Node));

	printf("Enter data\n");
	scanf("%d",&newNode->data);

	newNode->next=NULL;

	return newNode;

}

void addNode(){

	Node *newNode =createNode();

	if( head == NULL){

		head = newNode;
	}
	else{

		Node *temp =head;

		while( temp->next != NULL){

			temp=temp->next;
		}

		temp->next=newNode;
	}
}

int push(){

	if(isFull()){

		return -1;
	}
	else{
		addNode();
		return 0;
	}
}

int deleteLast(){

	if( head->next = NULL){
		free(head);
		head=NULL;
	}

	else{

		Node *temp = head;

		while(temp->next->next != NULL){

			temp=temp->next;
		}

		int data = temp->data;
		free(temp->next);
		temp->next=NULL;
		return data;
	}
}

bool isEmpty(){

	if( head == NULL){
		return true;
	}

	else{
		return false;
	}
}

int pop(){

	if( isEmpty()){

		flag = 0;
		return -1;
	}

	else{

		int val= deleteLast();
		flag =1;
		return val;
	
	}
}
int peek(){

	if(isEmpty()){

		flag =0;
		return -1;
	}

	else{
		int val = deleteLast();
		flag =1;
		return val;
	}
}

			
void main(){

	printf("Enter size of stack\n");
	scanf("%d",&countNode);


	 char choice;

        do{
                printf("******stack operation*****\n");
                printf("1.push\n");
                printf("2.pop\n");
                printf("3.peek\n");
                printf("4.isEmpty\n");
                printf("5.isFull\n");
                printf("Enter choice\n");
                int ch;
                scanf("%d",&ch);


                switch(ch){


                        case 1:
                               {
                                int ret;
                                ret = push();
                                if(ret == -1){
                                        printf("Stack Overflow\n");
                                }
                               }
                               break;


                       case 2 :{

                              int ret;
                              ret = pop();
                              if( flag == 0){
                                  printf("Stack underflow\n");
                              }
                              break;
                              }

                       case 3 :{

                              int ret;
                              ret = peek();
                              if( flag == 0){
                                  printf("Stack Empty\n");
                              }

                              else{

                                      printf("%d",ret);
                              }
                              break;
                              }

                       case 4:

                              isEmpty();
                              break;

                       case 5:

                              isFull();
                              break;

                      default :

                               printf("Wrong choice\n");
                               break;
                }
                 getchar();
                 printf("Do you want to continue\n");
                 scanf("%c",&choice);

                }while( choice == 'Y' || choice == 'y');

}
