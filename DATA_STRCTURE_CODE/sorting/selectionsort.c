#include<stdio.h>

void selection(int arr[], int size) {

    for (int i = 0; i < size-1; i++) {

        int minIndex = i;

        for (int j = i+1; j < size; j++) {
            if (arr[j] < arr[minIndex]) {
                minIndex = j;
            }
        }
        int temp = arr[i];
        arr[i] = arr[minIndex];
        arr[minIndex] = temp;
    }
}





void main(){


        int size;
        printf("Enter array size\n");
        scanf("%d",&size);
        int arr[size];

        printf("Enter array elements\n");

        for( int i =0; i<size; i++){

                scanf("%d",&arr[i]);


        }

        printf("After sort\n");

        selection(arr,size);

        for( int i =0; i<size; i++){

                printf("%d\n",arr[i]);
        }

}
