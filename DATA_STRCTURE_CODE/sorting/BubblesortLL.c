#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node *next;
}Node;

Node *head=NULL;

Node* createNode(){

	Node *newNode = (Node*)malloc(sizeof(Node));
	printf("Enter data\n");
        scanf("%d",&newNode->data);

	newNode->next=NULL;

	return newNode;

}

void addNode(){

	Node *newNode =createNode();

	if( head == NULL){

		head = newNode;

	}

	else{

		Node *temp=head;

		while(temp ->next != NULL){
			temp =temp->next;
		}
		temp->next=newNode;
	}
}

void swap(Node *temp1, Node *temp2) {

    int temp = temp1->data;
    temp1->data = temp2->data;
    temp2->data = temp;
}


void sortList( ) {
   
	int swapele;

    Node *ptr1;
    Node *ptr2 = NULL;

    if (head == NULL)
        return;

    do {
        swapele = 0;
        ptr1 = head;

        while (ptr1->next != ptr2){

            if (ptr1->data > ptr1->next->data) {

                swap(ptr1, ptr1->next);

                swapele = 1;
            }
            ptr1 = ptr1->next;
        }
        ptr2= ptr1;
    } while (swapele);
}
int printNode(){

	  if(head == NULL){

		  printf("Linkedlist is empty\n");
		  return -1;
	  }
	  else{
		  Node *temp=head;
		  while(temp->next !=NULL){
			  printf("|%d->",temp->data);
			  temp=temp->next;
		  }
		  printf("|%d|",temp->data);

	  }

	  return 0;
}

void main (){

	int loop;
	printf("Enter node do you want to add\n");
	scanf("%d",&loop);

	for( int i =1; i<=loop; i++){

		addNode();

	}
	
	printNode();
	printf("\n");
        sortList();

        printf("Sorted Linked List: ");
        printNode();

}

