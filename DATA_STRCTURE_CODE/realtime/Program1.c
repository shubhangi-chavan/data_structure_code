/* singly realtime*/
 
#include<stdio.h>
#include<stdlib.h>

typedef struct Waterpark{
        
	char Wname[20];
	int rides;
	float Tprice;
	struct Waterpark *next;
}Wp;

Wp *head=NULL;

Wp* createNode(){

	Wp *newNode = (Wp*)malloc(sizeof(Wp));
	getchar();
	printf("Enter name of waterpark\n");
	int i=0;
	char ch;

	while( (ch= getchar()) != '\n'){

		(*newNode).Wname[i]=ch;
		i++;
	}

	printf("Enter no of rides\n");
	scanf("%d",&newNode->rides);
	printf("Enter Ticket price\n");
	scanf("%f",&newNode->Tprice);

	newNode->next=NULL;

	return newNode;

}

void addNode(){

	Wp *newNode =createNode();

	if( head == NULL){

		head = newNode;

	}

	else{

		Wp *temp=head;

		while(temp ->next != NULL){
			temp =temp->next;
		}
		temp->next=newNode;
	}
}
void addFirst(){

	Wp *newNode= createNode();

	if( head == NULL){

		head=newNode;

	}
	else{

		newNode->next=head;
		head=newNode;
	}
}
int countNode(){

        Wp *temp =head;
        int count =0;

        while( temp != NULL){

                temp=temp->next;
                count++;
        }
        printf("count of nodes %d\n :",count);

        return count;
}

int addAtpos( int pos){

	  int count=countNode();

	  if( pos <=0 || pos >= count+2){
		  printf("Invalid node position\n");
		  return -1;
	  }
	  else{

		  if( pos == count+1){

			  addNode();
		  }
		  else if ( pos == 1){

			  addFirst();
		  }
		  else{

			  Wp *newNode=createNode();
			  Wp * temp=head;
			  while(pos-2){

				  temp=temp->next;
				  pos--;
			  }
			  newNode->next=temp->next;
			  temp->next=newNode;
		  }


	  }

	  return 0;
}
int printNode(){

	  if(head == NULL){

		  printf("Linkedlist is empty\n");
		  return -1;
	  }
	  else{
		  Wp *temp=head;
	   while( temp->next != NULL){
		   printf("|%s ",temp->Wname);
	           printf("|%d ",temp->rides);
		   printf("|%f|->",temp->Tprice);
		   temp=temp->next;
	   }
	   printf("|%s ",temp->Wname);
	   printf("|%d ",temp->rides);
	   printf("|%f|",temp->Tprice);
	  }
	  return 0;
}

void deleteFirst(){

	int count=countNode();

	if(head==NULL){
		printf("No node to delete\n");
	}
	else if(count == 1){
		free(head);
		head=NULL;
	}
	else{

        Wp *temp =head;
        head =head->next;
        free(temp);
}
}
void deleteLast(){

        Wp *temp =head;

        while( temp->next->next != NULL){

                temp = temp->next;
        }
        free(temp->next);
        temp->next=NULL;
}
int deleteAtpos( int pos){

	int count = countNode();

	if( pos <= 0 || pos<count){

	       printf("Invalid node position\n");
	       return -1;

	}
        else{

		if( pos == 1){

			deleteFirst();
		}
	        else if(pos == count+1){

			deleteLast();
		}

		else{

			Wp *temp=head;
		

			while( pos-2){

				temp=temp->next;
				
			}
			 Wp* temp2=temp->next;
			temp->next=temp->next->next;
			free(temp2);
		}
	}
	return 0;
}


void main(){

	char choice;

	do{

		printf("\nLinkedList operation\n");
		printf("1.addNode\n");
		printf("2.addAtpos\n");
		printf("3.addFirst\n");
		printf("4.deleteFisrt\n");
		printf("5.deleteLast\n");
		printf("6.deleteAtpos\n");
		printf("7.printNode\n");
		printf("8.contNode\n");
		printf("Enter choice:\n");
		int ch;
		scanf("%d",&ch);


		switch( ch ){

			case 1:

				addNode();
				break;

			case 2:
			      {

				int pos;
			        printf("Enter position\n");
			        scanf("%d",&pos);
			        addAtpos(pos);
			      }
		              break;

		       case 3 :

			       addFirst();
			       break;

		       case 4 :

		               deleteFirst();
		               break;

		      case 5:

		              deleteLast();
	                      break;

		      case 6 :
			      {
			      int pos;
                              printf("Enter position\n");
                              scanf("%d",&pos);
		              deleteAtpos(pos);
			      }
	                      break;

	              case 7 :

			      printNode();
			      break;
	
	              case 8 :

			      countNode();
			      break;		     



	              default:

	                  printf("Invalid input\n");
                          break;
		}
                getchar();
		printf("Do you want to continue ?\n");
		scanf("%c",&choice);
	}while( choice == 'Y'|| choice == 'y');
}
