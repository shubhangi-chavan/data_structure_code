/* Doubly linkedlist*/
 
#include<stdio.h>
#include<stdlib.h>

typedef struct Kho_Kho{
        
        struct Kho_Kho *prev;
	char Pname[20];
	int jerno;
        struct Kho_Kho *next;
}Kho;

Kho *head=NULL;

Kho* createNode(){

	Kho *newNode = (Kho*)malloc(sizeof(Kho));
	getchar();
	 
	Kho *prev = NULL;

	printf("Enter name of player\n");
	int i=0;
	char ch;

	while( (ch= getchar()) != '\n'){

		(*newNode).Pname[i]=ch;
		i++;
	}

	printf("Enter jearNo\n");
	scanf("%d",&newNode->jerno);
	
	Kho *next=NULL;

	newNode->next=NULL;

	return newNode;

}

void addNode(){

	Kho *newNode =createNode();

	if( head == NULL){

		head = newNode;

	}

	else{

		Kho *temp=head;

		while(temp ->next != NULL){
			temp =temp->next;
		}
		temp->next=newNode;
		newNode->prev=temp;
	}
}
void addFirst(){

          Kho *newNode= createNode();

	if( head == NULL){

		head=newNode;

	}
	else{

		newNode->next=head;
		head->prev=newNode;
		head=newNode;
	}
}
int countNode(){

        Kho *temp =head;
        int count =0;

        while( temp != NULL){

                temp=temp->next;
                count++;
        }
        printf("count of nodes %d\n :",count);

        return count;
}

int addAtpos( int pos){

	  int count=countNode();

	  if( pos <=0 || pos >= count+2){
		  printf("Invalid node position\n");
		  return -1;
	  }
	  else{

		  if( pos == count+1){

			  addNode();
		  }
		  else if ( pos == 1){

			  addFirst();
		  }
		  else{

			  Kho *newNode=createNode();
			  Kho * temp=head;
			  while(pos-2){

				  temp=temp->next;
				  pos--;
			  }
			  newNode->next=temp->next;
			  newNode->prev=temp;
			  temp->prev->next=newNode;
			  temp->next=newNode;
		  }


	  }

	  return 0;
}
int printNode(){

	  if(head == NULL){

		  printf("Linkedlist is empty\n");
		  return -1;
	  }
	  else{
		  Kho *temp=head;
	   while( temp->next != NULL){
		   printf("|%s ",temp->Pname);
	           printf("|%d ",temp->jerno);
		   temp=temp->next;
	   }
	   printf("|%s ",temp->Pname);
	   printf("|%d ",temp->jerno);
	   
	  }
	  return 0;
}

void deleteFirst(){

	int count=countNode();

	if(head==NULL){
		printf("No node to delete\n");
	}
	else if(count == 1){
		free(head);
		head=NULL;
	}
	else{

        head =head->next;
        free(head->prev);
        head->prev=NULL;
}
}
void deleteLast(){

        Kho *temp =head;

        while( temp->next->next != NULL){

                temp = temp->next;
        }
        free(temp->next);
        temp->next=NULL;
}
int deleteAtpos( int pos){

	int count = countNode();

	if( pos <= 0 || pos>count){

	       printf("Invalid node position\n");
	       return -1;

	}
        else{

		if( pos == 1){

			deleteFirst();
		}
	        else if(pos == count+1){

			deleteLast();
		}

		else{

			Kho *temp=head;
		

			while( pos-2){

				temp=temp->next;
				
			}
			 temp->next=temp->next->next;
			 free(temp->next->prev);
			 temp->next->prev=temp;
			
		}
	}
	return 0;
}


void main(){

	char choice;

	do{

		printf("\nLinkedList operation\n");
		printf("1.addNode\n");
		printf("2.addAtpos\n");
		printf("3.addFirst\n");
		printf("4.deleteFisrt\n");
		printf("5.deleteLast\n");
		printf("6.deleteAtpos\n");
		printf("7.printNode\n");
		printf("8.contNode\n");
		printf("Enter choice:\n");
		int ch;
		scanf("%d",&ch);


		switch( ch ){

			case 1:

				addNode();
				break;

			case 2:
			      {

				int pos;
			        printf("Enter position\n");
			        scanf("%d",&pos);
			        addAtpos(pos);
			      }
		              break;

		       case 3 :

			       addFirst();
			       break;

		       case 4 :

		               deleteFirst();
		               break;

		      case 5:

		              deleteLast();
	                      break;

		      case 6 :
			      {
			      int pos;
                              printf("Enter position\n");
                              scanf("%d",&pos);
		              deleteAtpos(pos);
			      }
	                      break;

	              case 7 :

			      printNode();
			      break;
	
	              case 8 :

			      countNode();
			      break;		     



	              default:

	                  printf("Invalid input\n");
                          break;
		}
                getchar();
		printf("Do you want to continue ?\n");
		scanf("%c",&choice);
	}while( choice == 'Y'|| choice == 'y');
}
