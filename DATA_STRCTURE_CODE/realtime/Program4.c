// Doubly-circular//

#include<stdio.h>
#include<stdlib.h>

typedef struct Song_Playlist{

	struct Song_Playlist  *prev;
	char Sname[20];
	int songNo;
	float rating;
	struct Song_Playlist *next;
}List;

 List *head = NULL;

List* createNode(){

         List *newNode=( List*)malloc(sizeof(List));
	 getchar();

	 newNode->prev=NULL;

	printf("song name\n");
	

	int i=0;
	char ch;

	while( (ch= getchar()) != '\n'){

		(*newNode).Sname[i]=ch;
		i++;
	}

	printf("Enter song number\n");
	scanf("%d",&newNode->songNo);
	printf("Enter song rating\n");
	scanf("%f",&newNode->rating);
	

	newNode->next=NULL;

	return newNode;

}

void addNode(){

	 List *newNode = createNode();

	if( head==NULL){

      		head=newNode;
		newNode->next=head;
		newNode->prev=head;

	}

	else{



		head->prev->next=newNode;
		newNode->prev=head->prev;
		newNode->next=head;
		head->prev=newNode;

	}
}

int countNode(){

	 List *temp =head;

	 if(head==NULL){
		 printf("No node present\n");
		 return 0;
	 }
	 else if(head->next==head){
		 return 1;
	 }
	 else{

	int count=1;

	while( temp->next != head){


		temp=temp->next;
		count++;

	}
	printf("%d",count);
	return count;
}
}


 void printNode(){

	 if( head==NULL){

		 printf("Empty linkedList");
	 }
	 else{

	 List *temp=head;

	while( temp->next != head){
                
		printf("|%s|->",temp->Sname);
		printf("|%d|->",temp->songNo);
		printf("|%f|->",temp->rating);
		temp=temp->next;
	}
	        printf("|%s|->",temp->Sname);
                printf("|%d|->",temp->songNo);
                printf("|%f|->",temp->rating);
}
}
void addFirst(){

	List *newNode = createNode();

	if( head==NULL){


		head=newNode;
		newNode->next=head;
		newNode->prev=head;
	}
	else{
		newNode->next=head;
		head->prev->next=newNode;
		newNode->prev=head->prev;
		head=newNode;


	}
}




int addAtpos( int pos){

	int count=countNode();

	if( pos <= 0 || pos >= count+2){
		 printf("Invalid position\n");
		 return -1;
	}
	else{
		if(pos == 1){
			addFirst();
		}
		else if(pos == count+1){
			addNode();
		}
		else{
                        List *newNode=createNode();
			List *temp=head;

			while( pos-2){

				temp=temp->next;
				pos--;
			}
			newNode->next=temp->next;
	                newNode->prev=temp;
			temp->next->prev=newNode;
			temp->next=newNode;
		}
	      return 0;
	}
}

int deleteFirst(){

	int count=countNode();

	if(head==NULL){

		printf("Nothing to delete\n");
		return -1;
	}
	else if(count == 1){
		free(head);
		head=NULL;
	}
	else{
		head->next->prev=head->prev;
		head=head->next;
		free(head->prev->next);
		head->prev->next=head;

	}

	return 0;
}

int deleteLast(){

	int count=countNode();

	if(head==NULL){
		printf("Empty list\n");
		return -1;
	}
	else if(count == 1){
		free(head);
		head=NULL;
	}
	else{

		head->prev=head->prev->prev;
		free(head->prev->next);
		head->prev->next=head;
	}
	return 0;
}

int deleteAtpos(int pos){



	int count = countNode();

	if( pos <= 0 || pos > count){

	       printf("Invalid node position\n");
	       return -1;

	}
        else{

		if( pos == 1){

			deleteFirst();
		}
	        else if(pos == count+1){

			deleteLast();
		}

		else{
			List *temp=head;
			while( pos -2){

				temp=temp->next;
				pos--;
			}


		     temp->next=temp->next->next;
		     free(temp->next->prev);
		     temp->next->prev=temp;

         	}

	}
	return 0;
}



void main(){


	char choice;

	do{

		printf("\nLinkedList operation\n");
		printf("1.addNode\n");
		printf("2.addAtpos\n");
		printf("3.addFirst\n");
		printf("4.deleteFisrt\n");
		printf("5.deleteLast\n");
		printf("6.deleteAtpos\n");
		printf("7.printNode\n");
		printf("Enter choice:\n");
		int ch;
		scanf("%d",&ch);


		switch( ch ){

			case 1:

				addNode();
				break;

			case 2:
			      {

				int pos;
			        printf("Enter position\n");
			        scanf("%d",&pos);
			        addAtpos(pos);
			      }
		              break;

		       case 3 :

			       addFirst();
			       break;

		       case 4 :

	                       deleteFirst();
		               break;

		      case 5:

		              deleteLast();
	                      break;

		      case 6:
                              {

                                int pos;
                                printf("Enter position\n");
                                scanf("%d",&pos);
                                deleteAtpos(pos);
                              }
                              break;

		      case 7 :

		             printNode();
	                     break;





	              default:

	                  printf("Invalid input\n");
                          break;
		}
                getchar();
		printf("Do you want to continue ?\n");
		scanf("%c",&choice);
	}while( choice == 'Y'|| choice == 'y');
}
