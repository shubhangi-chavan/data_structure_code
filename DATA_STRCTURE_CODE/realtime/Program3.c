// Singlycircular

#include<stdio.h>
#include<stdlib.h>

typedef struct Giant_wheel {

	int rideNo;
	float Tprice;
	struct Giant_wheel *next;
}Gw;

 Gw *head = NULL;

Gw* createNode(){

         Gw *newNode=( Gw*)malloc(sizeof(Gw));

	printf("Enter ride number to seat in Giant-wheel\n");
	scanf("%d",&newNode->rideNo);
	printf("Enter ticket price\n");
	scanf("%f",&newNode->Tprice);

	newNode->next=NULL;

	return newNode;

}

void addNode(){

	 Gw *newNode = createNode();

	if( head==NULL){

      		head=newNode;
		newNode->next=head;

	}

	else{

		 Gw *temp=head;

		while( temp->next != head){

			temp=temp->next;

		}

		temp->next=newNode;
		newNode->next=head;

	}
}

int countNode(){

	 Gw *temp =head;

	 if(head==NULL){
		 printf("No node present\n");
		 return 0;
	 }
	 else if(head->next==head){
		 return 1;
	 }
	 else{

	int count=1;

	while( temp->next != head){


		temp=temp->next;
		count++;

	}
	printf("%d",count);
	return count;
}
}


 void printNode(){

	 if( head==NULL){

		 printf("Empty linkedList");
	 }
	 else{

	 Gw *temp=head;

	while( temp->next != head){

		printf("|%d|->",temp->rideNo);
		printf("|%f|->",temp->Tprice);
		temp=temp->next;
	}
	printf("|%d|",temp->rideNo);
	printf("|%f|",temp->Tprice);
}
}
void addFirst(){

	Gw *newNode = createNode();

	if( head==NULL){


		head=newNode;
		newNode->next=head;
	}
	else{
		Gw* temp =head;

		while( temp->next != head){

			temp=temp->next;


		}



		newNode->next=head;
		head=newNode;
		temp->next=head;


	}
}




int addAtpos( int pos){

	int count=countNode();

	if( pos <= 0 || pos >= count+2){
		 printf("Invalid position\n");
		 return -1;
	}
	else{
		if(pos == 1){
			addFirst();
		}
		else if(pos == count+1){
			addNode();
		}
		else{
                        Gw *newNode=createNode();
			Gw *temp=head;

			while( pos-2){

				temp=temp->next;
				pos--;
			}
			newNode->next=temp->next;
			temp->next=newNode;
		}
	      return 0;
	}
}

int deleteFirst(){

	int count=countNode();

	if(head==NULL){

		printf("Nothing to delete\n");
		return -1;
	}
	else if(count == 1){
		free(head);
		head=NULL;
	}
	else{
		Gw *temp1 =head;
		Gw *temp2=head;

		while(temp2->next != head){

			temp2=temp2->next;
		}


		temp2->next=temp1->next;
		head=head->next;
		free(temp1);

	}

	return 0;
}

int deleteLast(){

	int count=countNode();

	if(head==NULL){
		printf("Empty list\n");
		return -1;
	}
	else if(count == 1){
		free(head);
		head=NULL;
	}
	else{
		Gw *temp1 =head;
		Gw *temp2 =head;
		while(temp1->next->next != head){
			temp1=temp1->next;
		}


		free(temp1->next);
		temp1->next=temp2;
	}
	return 0;
}

int deleteAtpos(int pos){



	int count = countNode();

	if( pos <= 0 || pos > count){

	       printf("Invalid node position\n");
	       return -1;

	}
        else{

		if( pos == 1){

			deleteFirst();
		}
	        else if(pos == count+1){

			deleteLast();
		}

		else{

			Gw *temp=head;


			while( pos-2){

				temp=temp->next;

				pos--;
			}
                        Gw *temp2=temp->next;
			temp->next=temp->next->next;
			free(temp2);


		}
	}
	return 0;
}



void main(){


	char choice;

	do{

		printf("\nLinkedList operation\n");
		printf("1.addNode\n");
		printf("2.addAtpos\n");
		printf("3.addFirst\n");
		printf("4.deleteFisrt\n");
		printf("5.deleteLast\n");
		printf("6.deleteAtpos\n");
		printf("7.printNode\n");
		printf("Enter choice:\n");
		int ch;
		scanf("%d",&ch);


		switch( ch ){

			case 1:

				addNode();
				break;

			case 2:
			      {

				int pos;
			        printf("Enter position\n");
			        scanf("%d",&pos);
			        addAtpos(pos);
			      }
		              break;

		       case 3 :

			       addFirst();
			       break;

		       case 4 :

	                       deleteFirst();
		               break;

		      case 5:

		              deleteLast();
	                      break;

		      case 6:
                              {

                                int pos;
                                printf("Enter position\n");
                                scanf("%d",&pos);
                                deleteAtpos(pos);
                              }
                              break;

		      case 7 :

		             printNode();
	                     break;





	              default:

	                  printf("Invalid input\n");
                          break;
		}
                getchar();
		printf("Do you want to continue ?\n");
		scanf("%c",&choice);
	}while( choice == 'Y'|| choice == 'y');
}
