#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node *next;
}Node;

Node *head=NULL;

Node* createNode(){

	Node *newNode = (Node*)malloc(sizeof(Node));
	printf("Enter data\n");
        scanf("%d",&newNode->data);

	newNode->next=NULL;

	return newNode;

}

void addNode(){

	Node *newNode =createNode();

	if( head == NULL){

		head = newNode;

	}

	else{

		Node *temp=head;

		while(temp ->next != NULL){
			temp =temp->next;
		}
		temp->next=newNode;
	}
}
void addFirst(){

	Node *newNode= createNode();

	if( head == NULL){

		head=newNode;

	}
	else{

		newNode->next=head;
		head=newNode;
	}
}
int countNode(){

        Node *temp =head;
        int count =0;

        while( temp != NULL){

                temp=temp->next;
                count++;
        }
        printf("count of nodes %d\n :",count);

        return count;
}

int addAtpos( int pos){

	  int count=countNode();

	  if( pos <=0 || pos >= count+2){
		  printf("Invalid node position\n");
		  return -1;
	  }
	  else{

		  if( pos == count+1){

			  addNode();
		  }
		  else if ( pos == 1){

			  addFirst();
		  }
		  else{

			  Node *newNode=createNode();
			  Node * temp=head;
			  while(pos-2){

				  temp=temp->next;
				  pos--;
			  }
			  newNode->next=temp->next;
			  temp->next=newNode;
		  }


	  }

	  return 0;
}
int printNode(){

	  if(head == NULL){

		  printf("Linkedlist is empty\n");
		  return -1;
	  }
	  else{
		  Node *temp=head;
		  while(temp->next !=NULL){
			  printf("|%d->",temp->data);
			  temp=temp->next;
		  }
		  printf("|%d|",temp->data);
	  }
}

void deleteFirst(){

	int count=countNode();

	if(head==NULL){
		printf("No node to delete\n");
	}
	else if(count == 1){
		free(head);
		head=NULL;
	}
	else{

        Node *temp =head;
        head =head->next;
        free(temp);
}
}
void deleteLast(){

        Node *temp =head;

        while( temp->next->next != NULL){

                temp = temp->next;
        }
        free(temp->next);
        temp->next=NULL;
}
int deleteAtpos( int pos){

	int count = countNode();

	if( pos <= 0 || pos<count){

	       printf("Invalid node position\n");
	       return -1;

	}
        else{

		if( pos == 1){

			deleteFirst();
		}
	        else if(pos == count+1){

			deleteLast();
		}

		else{

			Node *temp=head;
		

			while( pos-2){

				temp=temp->next;
				//temp2=temp2->next->next;
			}
			 Node* temp2=temp->next;
			temp->next=temp->next->next;
			free(temp2);

		}
	}
	return 0;
}
int revLL(){

	if( head == NULL){
		printf("empty list\n");
		return -1;
	}
	else{
		Node *temp1=head;
		Node *temp2=head;

		while( temp2->next != NULL){
			temp2=temp2->next;
		}
		int count=countNode();
		int cnt=count/2;
		

		while(count){

			while(cnt){
				int temp=temp2->data;
				temp2->data=temp1->data;
				temp1->data=temp;
				temp1=temp1->next;
				temp2=head;
			        
				cnt--;
			
			}
			
			count--;
		}
	
	}
	  return 0;
}


int sortList(){

	Node *temp=head;
	Node *i = NULL;

	int temp2;

	if( head == NULL){

		return -1;
	}
	else{

		while( temp != NULL){

			i=temp->next;

			while( i != NULL){

				if( temp->data > i->data){

					temp2=temp->data;
					temp->data=i->data;
					i->data=temp2;
				}

				i=i->next;
			}

			temp=temp->next;
		}
	}
}





void main(){

	char choice;

	do{

		printf("\nLinkedList operation\n");
		printf("1.addNode\n");
		printf("2.addAtpos\n");
		printf("3.addFirst\n");
		printf("4.deleteFisrt\n");
		printf("5.deleteLast\n");
		printf("6.deleteAtpos\n");
		printf("7.printNode\n");
		printf("8.contNode\n");
		printf("9.revLL\n");
		printf("10.sortlist\n");
		printf("Enter choice:\n");
		int ch;
		scanf("%d",&ch);


		switch( ch ){

			case 1:

				addNode();
				break;

			case 2:
			      {

				int pos;
			        printf("Enter position\n");
			        scanf("%d",&pos);
			        addAtpos(pos);
			      }
		              break;

		       case 3 :

			       addFirst();
			       break;

		       case 4 :

		               deleteFirst();
		               break;

		      case 5:

		              deleteLast();
	                      break;

		      case 6 :
			      {
			      int pos;
                              printf("Enter position\n");
                              scanf("%d",&pos);
		              deleteAtpos(pos);
			      }
	                      break;

	              case 7 :

			      printNode();
			      break;
	
	              case 8 :

			      countNode();
			      break;

		     case 9:
		           
		             revLL();
	                     break;
	             
	             case 10:
	                    
	                      
	                     sortList();
	                     break;		     



	              default:

	                  printf("Invalid input\n");
                          break;
		}
                getchar();
		printf("Do you want to continue ?\n");
		scanf("%c",&choice);
	}while( choice == 'Y'|| choice == 'y');
}
