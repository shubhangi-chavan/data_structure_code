/*Program 5. Write a program that accepts two singly linear linked
list from the user and also accept range and concat elements of the
source singly linear linked list from that range after a singly linear
destination linked list.
Input source linked list : |30|->|30|->|70|->|80|->|90|->|100|
Input destination linked list : |30|->|40|
Input starting range: 2
Input ending range: 5
Output destination linked list : |30|->|40|->|30|->|70|->|80|-
>|90|*/


#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

        int data;
        struct Node *next;

}Node;

Node *head1 = NULL;
Node *head2 = NULL;

Node* createNode(){

        Node *newNode = (Node*)malloc(sizeof(Node));
        printf("Enter data\n");
        scanf("%d",&newNode->data);


        newNode->next=NULL;

        return newNode;



}

Node* addNode( struct Node *head){

        Node *newNode= createNode();

        if( head == NULL){

                head =newNode;
        }

        else{

                Node *temp =head;

                while(temp->next != NULL){

                                temp=temp->next;
                }
                temp->next=newNode;

        }
         return head;

}
int countNode( struct Node *head){

        Node *temp = head;
        int count=0;
        while( temp != NULL){

                count++;
                temp=temp->next;
        }
        return count;
}
void printNode( Node *head){

        Node *temp = head;

        while( temp ->next != NULL){
                printf("|%d->",temp->data);
                temp=temp->next;
        }
        printf("|%d|",temp->data);

}

void concat( Node * head1 , Node *head2 , int start , int end ){


	 Node* temp = head2;
            for (int i = 1; i < start; i++) {
               temp = temp->next;
      }

  // Insert the nodes from the start position to the end position
  // after the destination linked list
  Node* end2 = head1;
  while (end2->next != NULL) {
  
	  end2 = end2->next;
  }

  for (int i = start; i <= end; i++) {
  
	  end2->next = temp;
          end2 = end2->next;
          temp = temp->next;
  }

  // Set the next pointer of the end2 node to NULL
         end2->next = NULL;
}

void main(){

        int nodecount;
        printf("Enter node : L1\n");
        scanf("%d",&nodecount);

        for( int i=1; i<= nodecount; i++){

                head1 = addNode(head1);
        }

        printf("Enter node : L2\n");
        scanf("%d",&nodecount);

        for( int i =1; i<= nodecount; i++){

                head2 = addNode(head2);
        }

        printf("Before concat\n");
        printNode(head1);
        printf("\n");
        printNode(head2);

        int start , end;
        printf("\nEnter starting range :\n");
        scanf("%d",&start);
        printf("\nEnter end range :\n");
        scanf("%d",&end);
        printf("\n");
	concat( head1 , head2 , start ,end);
        printf("After concat\n");
        printNode(head1);

}





