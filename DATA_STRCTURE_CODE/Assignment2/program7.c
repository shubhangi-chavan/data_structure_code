/*Program 7. Write a program that copies the last N contents of the
source singly linear linked list to the destination singly linear linked list.
Input source linked list : |30|->|30|->|70|->|80|->|90|->|100|
Input no: 4
Output destination linked list : |70|->|80|->|90|->|100|*/

#include <stdio.h>
#include <stdlib.h>

 typedef struct Node {

    int data;
    struct Node* next;
}Node;

 typedef struct linked_list {
     
	 struct Node* head;
}List;

void addNode(List *list, int data) {

    // Add a new node with the given value to the end of the list. 

    Node *newNode = (Node*)malloc(sizeof(Node));

    

    newNode->data = data;

    newNode->next = NULL;

    if (list->head == NULL) {

        list->head = newNode;
	return;
        
    }
    Node *temp = list->head;

    while (temp->next != NULL) {

        temp = temp->next;
    }
    temp->next = newNode;
}




void copyLast(List *head2, List *head1 , int n) {

    // Copy the last n nodes of the source list to the destination list. 

    if (head2->head == NULL) {
        
    }
    
    Node *temp = head2->head;

    Node *lastNnode = temp;

    int count = 0;

    while (temp != NULL) {

        count++;

        temp = temp->next;

        if (count > n) {

            lastNnode = lastNnode->next;
        }
    }
    // Append the last n nodes of the source list to the destination list
    temp = lastNnode;

    while (temp!= NULL) {
        addNode(head1, temp->data);
        temp = temp->next;
    }
}

void main() {

    List source_list;
    source_list.head = NULL;
    
     int nodeCount;
    printf("Enter number of nodes you want\n");
    scanf("%d" ,&nodeCount);

    for( int i=1; i<=nodeCount; i++){


            int data;
            printf("Enter data : ");
            scanf("%d",&data);
            addNode(&source_list,data);
    }
    
     
    //print source Linkedlist
    Node *temp1 =   source_list.head;
    while (temp1 != NULL) {
        printf("%d\n", temp1->data);
        temp1 = temp1->next;
    }   
    

    List destination_list;

    destination_list.head = NULL;

    int num;
    printf("Enter number  of nodes to copy\n");
    scanf("%d",&num);
    copyLast(&source_list, &destination_list,num);

    // Print the destination list

    Node *temp = destination_list.head;
    while (temp != NULL) {
        printf("%d\n", temp->data);
        temp = temp->next;
    }

    
}
