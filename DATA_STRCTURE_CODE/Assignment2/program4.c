/* *Program 4. Write a program that accepts two singly linear linked
lists from the user and concat the last N elements of the source
linked list after the destination linked list.
Input source linked list : |30|->|30|->|70|
Input destination linked list : |10|->|20|->|30|->|40|
Input number of elements : 2
Output destination linked list : |10|->|20|->|30|->|40|->|30|-
>|70|*/



#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node *next;

}Node;

Node *head1 = NULL;
Node *head2 = NULL;

Node* createNode(){

	Node *newNode = (Node*)malloc(sizeof(Node));
	printf("Enter data\n");
	scanf("%d",&newNode->data);

	newNode->next=NULL;

	return newNode;

}

Node* addNode( struct Node *head){

	Node *newNode= createNode();

	if( head == NULL){

		head =newNode;
	}

	else{

		Node *temp =head;

		while(temp->next != NULL){

				temp=temp->next;
		}
		temp->next=newNode;

	}
	 return head;
	
} 
int countNode( struct Node *head){

        Node *temp = head;
	int count=0;
        while( temp != NULL){

                count++;
		temp=temp->next;
        }
        return count;
}

void concatNll( int num){

	int count = countNode(head2);

	if(( head2 != NULL) && ( num <= count)) {

		Node *temp1 = head1;
                 
		while( temp1->next != NULL){
			temp1 =temp1->next;
		}

		Node *temp2 = head2;

		int val=count-num;

		while(val){

			temp2 =temp2->next;
			val--;
		}
		temp1->next=temp2;
	}

	else{

		printf("invalid node count\n");
}
}


 
void printNode( Node *head){

	Node *temp = head;

	while( temp ->next != NULL){
		printf("|%d->",temp->data);
		temp=temp->next;
	}
	printf("|%d|",temp->data);
}



void main(){

	int nodecount;
	printf("Enter node : L1\n");
	scanf("%d",&nodecount);

	for( int i=1; i<= nodecount; i++){

		head1 = addNode(head1);
	}

	printf("Enter node : L2\n");
	scanf("%d",&nodecount);

	for( int i =1; i<= nodecount; i++){

		head2 = addNode(head2);
	}

	printf("Before concat\n");
	printNode(head1);
	printf("\n");
	printNode(head2);
        
        int num;
	printf("\nEnter number of nodes to concat :\n");
	scanf("%d",&num);
	concatNll(num);
	printf("\n");
	printf("After concat\n");
	printNode(head1);

}




