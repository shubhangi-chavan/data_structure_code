
/*Program 6. Write a program that copies the first N contents of a
singly linear source linked list to a destination singly linear linked
list.
Input source linked list : |30|->|30|->|70|->|80|->|90|->|100|
Input no: 4
Output destination linked list : |30|->|30|->|70|->|80|*/


#include <stdio.h>
#include <stdlib.h>

 typedef struct Node {
    
	 int data;
         struct Node *next;
}Node;

 typedef struct linked_list {
     
	 struct Node *head;
}List;


void addNode(List *list, int data) {

    // Add a new node with the given value to the end of the list.

    Node *newNode = (Node*)malloc(sizeof(Node));



    newNode->data = data;

    newNode->next = NULL;

    if (list->head == NULL) {

        list->head = newNode;
        return;

    }
    Node *temp = list->head;

    while (temp->next != NULL) {

        temp = temp->next;
    }
    temp->next = newNode;
}


void copyFirst( List *head1, List *head2, int n) {
  
      	Node *temp = head1->head;
        
	int i = 0;
    while (temp != NULL && i < n) {
        addNode(head2, temp->data);
        temp = temp->next;
        i++;
    }
}
void main() {


    // Create the source linked list
    List source_list;
    source_list.head = NULL;
    
    int nodeCount;
    printf("Enter number of nodes you want\n");
    scanf("%d" ,&nodeCount);

    for( int i=1; i<=nodeCount; i++){
            

	    int data;
	    printf("Enter data : ");
	    scanf("%d",&data);
	    addNode(&source_list,data);
    }
    
    //printLinkedlist 
    Node *temp1 =   source_list.head;
    while (temp1 != NULL) {
        printf("%d\n", temp1->data);
        temp1 = temp1->next;
    }

    // Create the destination linked list
    List dest_list;
    dest_list.head = NULL;

    // Copy the first N nodes from the source list to the destination list
    int num;
    printf("Enter number  of nodes to copy\n");
    scanf("%d",&num);
    copyFirst(&source_list, &dest_list, num);
 
    
    // print linkedList
    Node *temp = dest_list.head;
    while (temp != NULL) {
        printf("%d\n", temp->data);
        temp = temp->next;
    }


    }



