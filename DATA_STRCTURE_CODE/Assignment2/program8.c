/*Program 8. Write a program that copies the contents of the source
singly linear linked list to the destination singly linear linked list
which lies between the particular range accepted by the user.
Input source linked list : |30|->|30|->|70|->|80|->|90|->|100|
Input Enter starting range: 2
Input Enter starting range: 5
Output destination linked list : |30|->|70|->|80|->|90|*/


#include <stdio.h>
#include <stdlib.h>

 typedef struct Node {

    int data;
    struct Node* next;
}Node;

 typedef struct linked_list {

         struct Node* head;
}List;

void addNode(List *list, int data) {

    // Add a new node at  the end of the list.

    Node *newNode = (Node*)malloc(sizeof(Node));



    newNode->data = data;

    newNode->next = NULL;

    if (list->head == NULL) {

        list->head = newNode;
        return;

    }
    Node *temp = list->head;

     while (temp->next != NULL ) {

        temp = temp->next;
    }
    temp->next = newNode;

}

  List* copyBetweenRange( struct linked_list *head2, int start, int end) {

   List *destination = (List*) malloc(sizeof(  List));

  destination->head = NULL;

   Node* temp = head2->head;

  int i = 1;

  while (temp) {

    if (i destination>= start && i <= end) {

      addNode(destination, temp->data);
    }

    temp = temp->next;
    i++;
  }

  return destination;
}

void main() {

   List source_list;
    source_list.head = NULL;

    int nodeCount;
    printf("Enter number of nodes you want\n");
    scanf("%d" ,&nodeCount);

    for( int i=1; i<=nodeCount; i++){


            int data;
            printf("Enter data : ");
            scanf("%d",&data);
            addNode(&source_list,data);
    }


   // print source Linkedlist
    Node *temp1 =   source_list.head;
    while (temp1 != NULL) {
        printf("%d\n", temp1->data);
        temp1 = temp1->next;
    }

   int start ;
  int end ;
  printf("Enter start\n");
  scanf("%d",&start);
  printf("Enter end\n");
  scanf("%d",&end);

  List* destination = copyBetweenRange(&source_list, start, end);

  //  the destination list

  Node *temp = destination->head;

  while (temp != NULL) {

    printf("%d\n", temp->data);
    temp = temp->next;
  }

  
}



