/*Program 1. Write a program that searches all occurrences of a
particular element from a singly linear linked list.
Input linked list : |10|->|20|->|30|->|40|->|30|->|30|->|70|
Input element: 30
Output : 3*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node *next;
}Node;

Node *head=NULL;

Node* createNode(){

	Node *newNode = (Node*)malloc(sizeof(Node));
	printf("Enter data\n");
        scanf("%d",&newNode->data);

	newNode->next=NULL;

	return newNode;

}

void addNode(){

	Node *newNode =createNode();

	if( head == NULL){

		head = newNode;

	}

	else{

		Node *temp=head;

		while(temp ->next != NULL){
			temp =temp->next;
		}
		temp->next=newNode;
	}
}

int occ( int num){

	Node *temp=head;

	int count=0;

	while(temp != NULL){

		if( temp->data == num){
			count++;
		}
		temp=temp->next;
	}
	if(count>0){
		printf("number is present %d times\n ",count);
	}
	else{
		printf("number not found\n");
	}
}
int printNode(){

	  if(head == NULL){

		  printf("Linkedlist is empty\n");
		  return -1;
	  }
	  else{
		  Node *temp=head;
		  while(temp->next !=NULL){
			  printf("|%d->",temp->data);
			  temp=temp->next;
		  }
		  printf("|%d|",temp->data);
	  }
}



void main(){


	char choice;

	do{

		printf("1.addNode\n");
		printf("2.occarance\n");
		printf("3.printNode\n");
		int ch;
		printf("Enter choice\n");
		scanf("%d",&ch);

		switch(ch){


			  case 1:

			         addNode();
				 break;

		          
		          case 2:
                                  {
				     int num=0;
				     printf("Enter number :\n");
				     scanf("%d",&num);
				     occ(num);
				  }
	                           break;		 

		         
		          case 3:
	                      
	                               printNode();
	                               break;		  


		          default:
				       printf("Invalid choice\n");
				       break;

	                       			       
	}
        getchar();
        printf( "Do you want to continue ?\n");
        scanf("%c",&choice);
       }while(choice == 'Y' || choice == 'y');
}		       
