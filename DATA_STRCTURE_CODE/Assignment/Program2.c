/*Program 6.
Write a program that accepts a singly linear linked list from the user.
Take a number from the user and print the data of the length of that
number. Length of kanha=5
Input: linked list: |Shashi |-> | Ashish|-> |Kanha |-> | Rahul |-> | Badhe |
Input: Enter number 5
Output:
Kanha
Rahul
Badhe*/

/*Program 7.
Write a program that accepts a singly linear linked list from the user.
Reverse the data elements from the linked list.
Input: linked list: |Shashi |-> | Ashish|-> |Kanha |-> | Rahul |-> | Badhe |
Output : linked list |ihsahS|-> |hsihsA|->|ahnaK|->|luhaR|->|ehdaB|*/

/*Program 8.
Write a program that accepts a singly linear linked list from the user.
Take a number from the user and only keep the elements that are equal in
length to that number and delete other elements. And print the Linked list
Length of Shashi = 6
Input: linked list: |Shashi |-> | Ashish|-> |Kanha |-> | Rahul |-> | Badhe |
Input: 6
Output : linked list: |Shashi |-> | Ashish|*/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct Node{

	char str[20];
	struct Node *next;
}Node;

Node *head =NULL;

Node* createNode(){

	Node *newNode=(Node*)malloc(sizeof(Node));
	getchar();

	printf("Enter name\n");
        char ch;
	int i=0;
	while((ch=getchar())!='\n'){
		(*newNode).str[i]=ch;
		i++;
	}
	newNode->next =NULL;
	return newNode;
}

void addNode(){

	Node *newNode=createNode();

	if(head==NULL){
		head=newNode;
	}
	else{
		 Node *temp=head;
		 while(temp->next!=NULL){
			 temp=temp->next;
		 }
		 temp->next=newNode;
	}
}

int lengthstr(int num){

	Node *temp=head;

	while(temp != NULL){

		if(strlen(temp->str)==num){
                    printf("%s\n",temp->str);
		}
		temp=temp->next;

	}
}

int printNode(){

	Node *temp=head;

	if( head ==NULL){
		printf("Linked list empty\n");
		return -1;
	}
	else{


	while(temp->next != NULL){

		printf("|%s|",temp->str);
		temp=temp->next;
	
	}
	printf("|%s|",temp->str);

	
}
}

char* Mystrrev(char* str){

	char *temp = str;
	while(*temp != '\0'){

		temp++;
	}
	temp--;
	char x;
	while( str<temp){
		x= *str;
		*str = *temp;
		*temp = x;
		str++;
		temp--;
	}
	return str;
}
void rev(){



 Node *temp=head;
    
 while(temp != NULL){

	 printf("|%s|",Mystrrev(temp->str));
	 temp=temp->next;
 }
}
int countNode(){

	Node *temp =head;
	int count =0;

	while( temp != NULL){

		temp=temp->next;
		count++;
	}
	printf("count of nodes %d\n :",count);

	return count;
}
void deleteFirst(){

	int count=countNode();
          
	if(head==NULL){
		printf("No node to delete\n");
	}
	else if(count == 1){
		free(head);
		head=NULL;
	}
	else{

        Node *temp =head;
        head =head->next;
        free(temp);
}
}


void deleteLast(){

        Node *temp =head;

        while( temp->next->next != NULL){

                temp = temp->next;
        }
        free(temp->next);
        temp->next=NULL;
}
int deleteAtpos( int pos){

	int count = countNode();

	if( pos <= 0 || pos<count){

	       printf("Invalid node position\n");
	       return -1;

	}
        else{

		if( pos == 1){

			deleteFirst();
		}
	        else if(pos == count+1){
            
			deleteLast();
		}

		else{

			Node *temp=head;
			Node *temp2=head;

			while( pos-2){

				temp=temp->next;
				temp2=temp2->next->next;
				pos--;
			}
			temp->next=temp->next->next;
			free(temp2);
			temp->next->next=NULL;
		
		}
	}
	return 0;
}

void deletedata( int len){

	Node *temp =head;
	int i=0,flag=0;

	while(temp != NULL){

		i++;
		if(strlen(temp->str) != len){
				deleteAtpos(i);
				flag=1;
				i--;
		}
		temp=temp->next;
	}
	if(flag==0){
		printf("all strings are equal to given length\n");

		printNode();
	}
}



void main(){

	char choice;

        do{

                printf("1.addNode\n");
                printf("2.lengthstr\n");
		printf("3.printNode\n");
		printf("4.strrev\n");
		printf("5.deltedata\n");
                
                int ch;
                printf("Enter choice\n");
                scanf("%d",&ch);

                switch(ch){


                          case 1:

                                 addNode();
                                 break;

                          case 2:
                                 {
                                  int num=0;
                                  printf("Enter number\n");
                                  scanf("%d",&num);
                                  lengthstr(num);
                                 }
                                  break;

                          case 3:
                                  
                                    
                                    printNode();
                                    break;

                          case 4:
			          
			          rev();
		                   break;
		          case 5:

		               {
		                int len;
	                        scanf("%d",&len);
				printf("Enter length\n");
	                        deletedata(len);
			       }
                               break;
			  default :
                                printf("Invalid choice\n");
                                break;				

		}
		
		printf("Do you want to continue ?\n");
		scanf(" %c",&choice);
		}while( choice == 'Y'|| choice == 'y');
	}


