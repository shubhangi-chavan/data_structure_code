/*Write a program that searches for the first occurrence of a particular
element from a singly linear linked list.
Input linked list: |10|->|20|->|30|->|40|->|50|->|30|->|70|
Input: Enter element: 30
Output : 3*/

/*Write a program that searches for the second last occurrence of a
particular element from a singly linear linked list.
Input linked list: |10|->|20|->|30|->|40|->|30|->|30|->|70|
Input Enter element: 30
Output: 5*/

/*Write a program that searches the occurrence of a particular element from
a singly linear linked list.
Input linked list: |10|->|20|->|30|->|40|->|50|->|30|->|70|
Input Enter element: 30
Output: 2 times*/


#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node *next;
}Node;

Node *head=NULL;

Node* createNode(){

	Node *newNode = (Node*)malloc(sizeof(Node));
	printf("Enter data\n");
        scanf("%d",&newNode->data);

	newNode->next=NULL;

	return newNode;

}

void addNode(){

	Node *newNode =createNode();

	if( head == NULL){

		head = newNode;

	}

	else{

		Node *temp=head;

		while(temp ->next != NULL){
			temp =temp->next;
		}
		temp->next=newNode;
	}
}

int firstOccurance(int num){

	
       int flag;
       int i=0;

	Node *temp =head;

	while( temp != NULL){
		
		if(temp->data==num){
			flag=0;
			printf("Eelement found at pos %d\n",i+1);
			break;
		}
		else{
			flag=1;
		}
		i++;
		temp=temp->next;
	}
	if(flag == 1){
		printf("element not found\n");
	               
	}
}


int secondLastoccurance(int num){


	Node *temp=head;

	int count=0,I1=0,I2=0;

	while( temp != NULL){
		count++;
		if( temp->data == num){
			I2=I1;
			I1=count;
		}
		temp=temp->next;
	}
	if(I1 == 0){
		printf("%d is not present\n",num);
	}

	else{
		if(I2){
			printf("%d is present and itd second last occ is %d : ",num,I2);
		}
	
	        else{
		printf("%d is present only once at index %d :",num ,I1);
	}
}
}


int occ( int num){

	Node *temp=head;

	int count=0;

	while(temp != NULL){
             
		if( temp->data == num){
			count++;
		}
		temp=temp->next;
	}
	if(count>0){
		printf("number is present %d times\n ",count);
	}
	else{
		printf("number not found\n");
	}
}


void sumofDigit(){
            
	         Node *temp=head;
		 

		 while(temp!=NULL){

			 
			 int temp1,sum=0,rem;
			 temp1=temp->data;
			 while(temp1!=0){

				 rem=temp1%10;
				 sum =sum+rem;
				 temp1=temp1/10;
				 temp->data=sum;
			 }
	                 printf("|%d|",temp->data);		 
	              	 temp=temp->next;
		}
		 
}

int palindrome(){

	    Node *temp=head;
	    int flag;
	    int i=0;

	    while(temp!=NULL){
                    
		    
		    int temp1,sum=0,rem;
		    temp1=temp->data;
		    while(temp1 != 0){

			    rem= temp1% 10;
		            sum=(sum*10)+rem;
			    temp1=temp1/10;
		    }
	    
	        
             
		   if(temp->data==sum){
			   flag=0;
			   printf("palindrome found at %d\n : ",i+1);
			   
			  
		   }
                   else{
			   flag=1;
		   }
		   i++;

                   temp=temp->next;
	    }
	    if(flag == 1){

		    printf("No palindrome present\n");
	    }

		    
		    
	    
}


			    

				 



int printNode(){
             
	  if(head == NULL){

		  printf("Linkedlist is empty\n");
		  return -1;
	  }
	  else{
		  Node *temp=head;
		  while(temp->next !=NULL){
			  printf("|%d->",temp->data);
			  temp=temp->next;
		  }
		  printf("|%d|",temp->data);
	  }
}



void main(){


	char choice;

	do{

		printf("1.addNode\n");
		printf("2.Firstoccurance\n");
		printf("3.SecondLastoccurance\n");
		printf("4.occarance\n");
		printf("5.printNode\n");
		printf("6.sumofDigits\n");
		printf("7.palindrome\n");
		int ch;
		printf("Enter choice\n");
		scanf("%d",&ch);

		switch(ch){


			  case 1:

			         addNode();
				 break;

		          case 2:
				 {
			          int num=0;
		                  printf("Enter number\n");
		                  scanf("%d",&num);		  
				  firstOccurance(num);
				 }
				  break;

		          case 3:
				  {
			            int num=0;
		                    printf("Enter number\n");
		                    scanf("%d",&num);

                                    secondLastoccurance(num);
				  }
				    break;

		          case 4:
                                  {
				     int num=0;
				     printf("Enter number :\n");
				     scanf("%d",&num);
				     occ(num);
				  }
	                           break;
	                  case 5:
	                      
	                               printNode();
	                               break;

		          case 6:
		                    
		                     sumofDigit();
	                             break;	
		          case 7:
		                   
		                    palindrome();
	                            break;			    
	                  
	              
			  default:
				       printf("Invalid choice\n");
				       break;

	                       			       
	}
        getchar();
        printf( "Do you want to continue ?\n");
        scanf("%c",&choice);
       }while(choice == 'Y' || choice == 'y');
}

        	
		                		 



	

