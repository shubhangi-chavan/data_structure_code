#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct TreeNode{
         

	int data;
	struct TreeNode *left;
	struct TreeNode *right;

}TreeNode;

typedef struct Queue{

	struct TreeNode *btNode;
	struct Queue *next;

}Que;

 Que *front = NULL;
 Que *rear =NULL;

bool isempty(){

	if( front == NULL && rear == NULL){

	return true;

	}else{
		return false;
	}
}

TreeNode* createNode( int level){

        level = level+1;

        TreeNode  *newNode = ( TreeNode*) malloc( sizeof( TreeNode));

        printf("Enter data for node : \n");
        scanf("%d",&(newNode->data));

        char ch;
        getchar();
        printf("Do you want to construct left binary tree for level : %d\n",level);
        scanf("%c",&ch);

        if(ch == 'y' || ch == 'Y')
                newNode->left=createNode(level);
        else
                newNode->left=NULL;

        getchar();
        printf("Do you want to construct right binary tree for level :%d\n",level);
        scanf("%c",&ch);

        if( ch == 'y'|| ch =='Y')
                newNode->right = createNode(level);
        else
                newNode->right=NULL;

        return newNode;
}
void enqueue( struct TreeNode *temp){

	
    struct Queue *newNode = (struct Queue*) malloc(sizeof(struct Queue));
    newNode->btNode = temp;
    newNode->next = NULL;

    if (isempty()) {

        front = rear = newNode;
    } else {

        rear->next = newNode;
        rear = newNode;
    }
}


struct TreeNode *dequeue(){

	
    if (isempty()) {
        printf("tree empty\n");
        
    } else {
        Que* temp = front;
        TreeNode* item = front->btNode;

        if (front == rear) {

            front = rear = NULL;
        } else {

            front = front->next;
            
        }
         free(temp);

        return item;
    }
}
int inOrder( TreeNode* root){

                if( root == NULL){
                        return 0;
                }
                inOrder(root->left);
                printf("%d ",root->data);
                inOrder(root->right);
        }
void Levelorder( struct TreeNode *root){

	TreeNode *temp = root;
	enqueue(temp);

	while( !isempty()){

		temp=dequeue();

		printf("%d ",temp->data);

		if( temp->left != NULL){

			enqueue( temp->left);
		}
		if(temp->right  != NULL){
			enqueue(temp->right);
		}
	}
}
void main(){

        TreeNode *root = (TreeNode*)malloc(sizeof(TreeNode));
        printf("\t*****************creating Binary tree******************\n");

        printf("Enter Data for root node :\n");
        scanf("%d",&(root->data));

        printf("Tree rooted with root : %d\n ",root->data);

        char ch;

        getchar();
        printf("Do you want to construct left binary tree for root: \n");
        scanf("%c",&ch);

        if( ch =='y'|| ch =='Y')
                root->left = createNode(0);
        else
                root->left=NULL;

        getchar();
        printf("Do you want to construct right binary tree for root: \n");
        scanf("%c",&ch);

        if(ch=='y'|| ch =='Y')
                root->right=createNode(0);
        else
                root->right=NULL;

        inOrder(root);
	printf("\n");
        Levelorder(root);
}






