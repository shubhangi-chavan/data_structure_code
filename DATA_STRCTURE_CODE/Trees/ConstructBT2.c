#include <stdio.h>
#include <stdlib.h>

struct TreeNode {
    int data;
    struct TreeNode *left;
    struct TreeNode *right;
};

struct TreeNode* constructBT(int inorder[], int postorder[], int istart, int iend, int poststart, int postend) {
    if (istart > iend || poststart > postend) {
        return NULL;
    }

    int rootData = postorder[postend];
    struct TreeNode *temp = malloc(sizeof(struct TreeNode));
    temp->data = rootData;

    int idx;
    for (idx = istart; idx <= iend; idx++) {
        if (inorder[idx] == rootData) {
            break;
        }
    }

    int Llength = idx - istart;

    temp->left = constructBT(inorder, postorder, istart, idx - 1, poststart, poststart + Llength - 1);
    temp->right = constructBT(inorder, postorder, idx + 1, iend, poststart + Llength, postend - 1);

    return temp;
}

void inOrder(struct TreeNode *root) {
    if (root == NULL) {
        return;
    }

    inOrder(root->left);
    printf("%d ", root->data);
    inOrder(root->right);


}
void preOrder(struct TreeNode *root) {
    if (root == NULL) {
        return;
    }

    printf("%d ", root->data);
    inOrder(root->left);
    inOrder(root->right);
}

void postOrder(struct TreeNode *root) {
    if (root == NULL) {
        return;
    }

    postOrder(root->left);
    postOrder(root->right);
    printf("%d ", root->data);
}

int main() {
    int inorder[] = {2, 5, 4, 1, 6, 3, 8, 7};
    int postorder[] = {5, 2, 4, 6, 8, 7, 3, 1};

    int istart = 0;
    int iend = 7;
    int poststart = 0;
    int postend = 7;

    struct TreeNode *root = constructBT(inorder, postorder, istart, iend, poststart, postend);

    printf("Inorder traversal: ");
    inOrder(root);
    printf("\n");

    printf("Postorder traversal: ");
    postOrder(root);
    printf("\n");

      printf("Preorder traversal: ");
    preOrder(root);
    printf("\n");


    return 0;
}

