#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include <limits.h>

struct BSTtreenode{

int data;
struct BSTtreenode* left;
struct BSTtreenode* right ;

};

struct BSTtreenode *insertNode( struct BSTtreenode *root , int data){

	if( root == NULL){

		struct BSTtreenode *newNode= malloc(sizeof(struct BSTtreenode));
		newNode->data=data;
		newNode->left=NULL;
		newNode->right=NULL;
		root=newNode;
		return root;

	}
	 if(root->data>data){

		root->left=insertNode(root->left,data);

	 }
	 else{
		 root->right=insertNode(root->right,data);
	 }
	 return root;

}
// Function to search for a node with a given key in the BST
//
bool searchNode(struct BSTtreenode* root, int ele) {

    if (root == NULL) {
        return false; // Key not found
    }
    if (root->data == ele) {
        return true; // Key found
    }

    // If the key is less than the current node's data, search in the left subtree
    if (ele < root->data) {
        return searchNode(root->left, ele);
    }
    
    // If the key is greater than the current node's data, search in the right subtree
    return searchNode(root->right, ele);
}

struct BSTtreenode* MaxNode(struct BSTtreenode* root) {
    if (root == NULL) {
        return NULL; // Tree is empty
    }
    while (root->right != NULL) {
        root = root->right;
    }
    return root;
}

struct BSTtreenode* MinNode(struct BSTtreenode* root) {
    if (root == NULL) {
        return NULL; // Tree is empty
    }
    while (root->left != NULL) {
        root = root->left;
    }
    return root;
}

struct BSTtreenode* deleteNode(struct BSTtreenode* root, int key) {
    if (root == NULL) {
        return root; // Node not found
    }

    if (key < root->data) {
        root->left = deleteNode(root->left, key);
    } else if (key > root->data) {
        root->right = deleteNode(root->right, key);
    } else {
        // Node with the key is found
        if (root->left == NULL) {
            struct BSTtreenode* temp = root->right;
            free(root);
            return temp;
        } else if (root->right == NULL) {
            struct BSTtreenode* temp = root->left;
            free(root);
            return temp;
      
    
 }
        // Node with two children: Get the inorder successor (smallest in the right subtree)
        struct BSTtreenode* temp = MinNode(root->right);
        root->data = temp->data; // Copy the inorder successor's data to this node
        root->right = deleteNode(root->right, temp->data); // Delete the inorder successor
    }
    return root;
}


void printTree(struct BSTtreenode* root) {
    if (root != NULL) {
        // Print the current node's data before traversing left and right subtrees
        printf("%d ", root->data);
        // Recursively traverse the left subtree
        printTree(root->left);
        // Recursively traverse the right subtree
        printTree(root->right);
    }
}

bool isBSTcheck(struct BSTtreenode* root, int* prev) {
    if (root == NULL) {
        return true;
    }

    // Recursively check the left subtree
    if (! isBSTcheck(root->left, prev)) {
        return false;
    }

    // Check if the current node's value is greater than the previous
    if (root->data <= *prev) {
        return false;
    }

    // Update the previous value to the current node's value
    *prev = root->data;

    // Recursively check the right subtree
    return isBSTcheck(root->right, prev);
}
bool isValidBST(struct BSTtreenode* root) {
    int prev = INT_MIN; // Initialize prev with the minimum possible integer value
    return isBSTcheck(root, &prev);
}

void main() {

    struct BSTtreenode* root = NULL;
    int choice;
    char ch;

    do {
        printf("Operations\n");
        printf("0. Print Node\n");
        printf("1. Insert Node\n");
        printf("2. Search Node\n");
	printf("3.max node \n");
	printf("4. Min node\n");
	printf("5.delete node\n");
	printf("6. Validate BST\n");
        printf("Enter choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 0:
                printTree(root);
                break;
            case 1: {
                int data;
                printf("Enter data: ");
                scanf("%d", &data);
                root = insertNode(root, data);
                break;
            }
            case 2: {
                int ele;
                printf("Enter element to search: ");
                scanf("%d", &ele);
                searchNode(root, ele);
                break;
            }
            
	    case 3: {
                
                struct BSTtreenode* maxNode = MaxNode(root);
                if (maxNode != NULL) {
                    printf("Maximum Node: %d\n", maxNode->data);
                } else {
                    printf("The tree is empty.\n");
                }
                break;
            }
            case 4: {
                
                struct BSTtreenode* minNode = MinNode(root);
                if (minNode != NULL) {
                    printf("Minimum Node: %d\n", minNode->data);
                } else {
                    printf("The tree is empty.\n");
                }
                break;
            }
            
             case 5: {
                int key;
                printf("Enter key to delete: ");
                scanf("%d", &key);
                root = deleteNode(root, key);
                break;
            }

            case 6: {
                bool isValid = isValidBST(root);
                if (isValid) {
                    printf("The tree is a valid BST.\n");
                } else {
                    printf("The tree is not a valid BST.\n");
                }
                break;
            }
		     

            		    
            default:
                printf("Invalid choice\n");
        }

        printf("Do you want to continue (Y/N): ");
        scanf(" %c", &ch); // Note the space before %c to consume the newline character

    } while (ch == 'Y' || ch == 'y');

    
}







