#include<stdio.h>
#include<stdlib.h>

typedef struct TreeNode{

	int data;
	struct TreeNode *left;
	struct TreeNode *right;
}TreeNode;


TreeNode *head = NULL;

int preOrder( TreeNode* root ){

	if( root == NULL){

		return 0;
	}

		printf("%d ",root->data);
		preOrder(root->left);
		preOrder(root->right);

	}

int inOrder( TreeNode* root){

		if( root == NULL){
			return 0;
		}
		inOrder(root->left);
		printf("%d ",root->data);
		inOrder(root->right);
	}
int postOrder( TreeNode *root){

	if( root == NULL){
		return 0;
	}
	postOrder(root->left);
	postOrder(root->right);
	printf("%d ",root->data);
}

TreeNode* createNode( int level){

	level = level+1;

	TreeNode  *newNode = ( TreeNode*) malloc( sizeof( TreeNode));

	printf("Enter data for node : \n");
	scanf("%d",&(newNode->data));

	char ch;
	getchar();
	printf("Do you want to construct left binary tree for level : %d\n",level);
	scanf("%c",&ch);

	if(ch == 'y' || ch == 'Y')
		newNode->left=createNode(level);
	else
		newNode->left=NULL;

	getchar();
	printf("Do you want to construct right binary tree for level :%d\n",level);
	scanf("%c",&ch);

	if( ch == 'y'|| ch =='Y')
		newNode->right = createNode(level);
	else
		newNode->right=NULL;

	return newNode;
}
void printTree( TreeNode *root){

	char ch;

	do{

		printf("\t ***********traversal**********\n");
		printf("1. preOrder\n");
		printf("2.inOrder\n");
		printf("3.postOrder\n");

		int choice;
		printf("Enter choice\n");
		scanf("%d",&choice);


		switch( choice){


                         case 1: printf("preorder Traversal: ");
				 preOrder(root);
				 break;
		         
		         case 2: printf("Inorder Traversal :  ");
	                         inOrder(root);
	                         break;

	                 case 3: printf("postOrder Traversal : ");
	                         postOrder(root);
	                         break;
		}

getchar();
printf("Do you want to continue ? \n");
scanf("%c",&ch);
}while( ch == 'y'|| ch =='Y');
}

int sizeOfBt( TreeNode* root){

	if( root == NULL)
		return 0;

	int leftsize =sizeOfBt(root->left);
	int rightsize = sizeOfBt( root->right);

	return leftsize+rightsize+1;
}
int sumOfBt( TreeNode* root){

        if( root == NULL)
                return 0;

        int leftsum =sumOfBt(root->left);
        int rightsum = sumOfBt( root->right);

        return leftsum+rightsum+root->data;

}
int maxof( int leftHeight,int rightHeight){

	if( leftHeight > rightHeight)
		return leftHeight;
	else
		return rightHeight;
}

int heightofBt( TreeNode* root){

	if(root ==NULL)
		return-1;

	int leftHeight = heightofBt(root->left);
	int rightHeight = heightofBt( root->right);

	return maxof(leftHeight,rightHeight)+1;
}
int sumofskipBt( TreeNode *root){

	if( root == NULL)
		return 0;

	int rightsum,leftsum;

	if( root ->left != NULL && root->right != NULL){

		leftsum=sumofskipBt(root->left);
		rightsum=sumofskipBt(root->right);
	}
}

void main(){

	TreeNode *root = (TreeNode*)malloc(sizeof(TreeNode));
	printf("\t*****************creating Binary tree******************\n");

	printf("Enter Data for root node :\n");
	scanf("%d",&(root->data));

	printf("Tree rooted with root : %d\n ",root->data);

	char ch;

	getchar();
	printf("Do you want to construct left binary tree for root: \n");
	scanf("%c",&ch);

	if( ch =='y'|| ch =='Y')
		root->left = createNode(0);
	else
		root->left=NULL;

	getchar();
	printf("Do you want to construct right binary tree for root: \n");
	scanf("%c",&ch);

	if(ch=='y'|| ch =='Y')
		root->right=createNode(0);
	else
		root->right=NULL;


	printTree(root);

	printf("Size of Binary tree : %d\n",sizeOfBt(root));
	printf("Sum of Binary tree : %d\n",sumOfBt(root));
	printf("Height of Binary tree : %d\n",heightofBt(root));

}


