#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct TreeNode{

	int data;
	struct TreeNode *left;
	struct TreeNode *right;
}TreeNode;

struct Stackframe {

	TreeNode *btNode;
	struct Stackframe *next;
};

 struct Stackframe *top = NULL;

bool isempty(){

	if( top == NULL){
		return true;
	}
	else{
		return false;
	}
}
void push( TreeNode *temp){

	struct Stackframe *newNode = ( struct  Stackframe*)malloc(sizeof( struct Stackframe));

	newNode->btNode =temp;
	newNode->next = top;

	top = newNode;

}
TreeNode *pop(){

	if(isempty()){
		printf("Tree is empty\n");
	}
	else{

		TreeNode *item =top->btNode;

		 struct Stackframe *temp = top;
		top=top->next;
		free(temp);

		return item;
	}
}
int inOrder( TreeNode* root){

                if( root == NULL){
                        return 0;
                }
                inOrder(root->left);
                printf("%d ",root->data);
                inOrder(root->right);
        }
void iterativeInorder( TreeNode *temp){

	while( ! isempty() || temp != NULL){

		if( temp != NULL){
			push(temp);
			temp=temp->left;
		}
		else{
			temp=pop();
			printf("%d ",temp->data);
			temp=temp->right;
		}
	}

} 
TreeNode* createNode( int level){

        level = level+1;

        TreeNode  *newNode = ( TreeNode*) malloc( sizeof( TreeNode));

        printf("Enter data for node : \n");
        scanf("%d",&(newNode->data));

        char ch;
        getchar();
        printf("Do you want to construct left binary tree for level : %d\n",level);
        scanf("%c",&ch);

        if(ch == 'y' || ch == 'Y')
                newNode->left=createNode(level);
        else
                newNode->left=NULL;

        getchar();
        printf("Do you want to construct right binary tree for level :%d\n",level);
        scanf("%c",&ch);

        if( ch == 'y'|| ch =='Y')
                newNode->right = createNode(level);
        else
                newNode->right=NULL;

        return newNode;
}
void main(){

        TreeNode *root = (TreeNode*)malloc(sizeof(TreeNode));
        printf("\t*****************creating Binary tree******************\n");

        printf("Enter Data for root node :\n");
        scanf("%d",&(root->data));

        printf("Tree rooted with root : %d\n ",root->data);

        char ch;

        getchar();
        printf("Do you want to construct left binary tree for root: \n");
        scanf("%c",&ch);

        if( ch =='y'|| ch =='Y')
                root->left = createNode(0);
        else
                root->left=NULL;

        getchar();
        printf("Do you want to construct right binary tree for root: \n");
        scanf("%c",&ch);

        if(ch=='y'|| ch =='Y')
                root->right=createNode(0);
        else
                root->right=NULL;


         iterativeInorder(root);
	 printf("\n");
	 inOrder(root);
}
