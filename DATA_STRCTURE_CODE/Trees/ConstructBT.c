#include <stdio.h>
#include <stdlib.h>

struct TreeNode {
    int data;
    struct TreeNode *left;
    struct TreeNode *right;
};

struct TreeNode* constructBT(int inorder[], int preorder[], int istart, int iend, int prestart, int preend) {
    if (istart > iend) {
        return NULL;
    }

    int rootData = preorder[prestart];
    struct TreeNode *temp = malloc(sizeof(struct TreeNode));
    temp->data = rootData;

    int idx;
    for (idx = istart; idx <= iend; idx++) {
        if (inorder[idx] == rootData) {
            break;
        }
    }

    int Llength = idx - istart;

    temp->left = constructBT(inorder, preorder, istart, idx - 1, prestart + 1, prestart + Llength);
    temp->right = constructBT(inorder, preorder, idx + 1, iend, prestart + Llength + 1, preend);

    return temp;
}

void inOrder(struct TreeNode *root) {
    if (root == NULL) {
        return;
    }

    inOrder(root->left);
    printf("%d ", root->data);
    inOrder(root->right);
}
void preOrder(struct TreeNode *root) {
    if (root == NULL) {
        return;
    }
    
    printf("%d ", root->data);
    inOrder(root->left);
    inOrder(root->right);
}

void postOrder(struct TreeNode *root) {
    if (root == NULL) {
        return;
    }

    postOrder(root->left);
    postOrder(root->right);
    printf("%d ", root->data);
}

int main() {
    int inorder[] = {2, 5, 4, 1, 6, 3, 8, 7};
    int preorder[] = {1, 2, 4, 5, 3, 6, 7, 8};

    int istart = 0;
    int iend = 7;
    int prestart = 0;
    int preend = 7;

    struct TreeNode *root = constructBT(inorder, preorder, istart, iend, prestart, preend);

    printf("Inorder traversal: ");
    inOrder(root);
    printf("\n");

    printf("Postorder traversal: ");
    postOrder(root);
    printf("\n");

     printf("Preorder traversal: ");
    preOrder(root);
    printf("\n");

    return 0;
}

