
#include<stdio.h>
#include<stdlib.h>

typedef struct waterpark{

	char Wname[20];
	int rides;
	float Tprice;
	struct waterpark *next;
}WK;

WK *head=NULL;

WK* createNode(){

	WK *newNode = (WK*)malloc(sizeof(WK));
	getchar();
	printf("Enter name of waterpark\n");
	int i=0;
	char ch;

	while( (ch= getchar()) != '\n'){

		(*newNode).Wname[i]=ch;
		i++;
	}

	printf("Enter no of rides\n");
	scanf("%d",&newNode->rides);
	printf("Enter Ticket price\n");
	scanf("%f",&newNode->Tprice);

	newNode->next=NULL;

	return newNode;
}




void addNode(){

	WK  *newNode = createNode();

	if( head == NULL){

		head = newNode;
	}
	else{

		WK *temp =head;

		while( temp ->next != NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}

void printNode(){

	   WK *temp=head;
	   while( temp->next != NULL){
		   printf("|%s ",temp->Wname);
	           printf("|%d ",temp->rides);
		   printf("|%f|->",temp->Tprice);
		   temp=temp->next;
	   }
	   printf("|%s ",temp->Wname);
	   printf("|%d ",temp->rides);
	   printf("|%f|",temp->Tprice);

}
void addFirst(){

	WK *newNode=createNode();

	if( head == NULL){

		head = newNode;
	}
	else{

		newNode->next=head;
		head=newNode;
	}
}

int countNode(){

	WK *temp =head;
	int count =0;

	while( temp != NULL){

		temp=temp->next;
		count++;
	}
	printf("count of nodes %d\n :",count);

	return count;
}

int addAtpos( int pos){
         
	  int count=countNode();

	  if( pos <=0 || pos >= count+2){
		  printf("Invalid node position\n");
		  return -1;
	  }
	  else{

		  if( pos == count+1){

			  addNode();
		  }
		  else if ( pos == 1){

			  addFirst();
		  }
		  else{

			  WK *newNode=createNode();
			  WK * temp=head;
			  while(pos-2){

				  temp=temp->next;
				  pos--;
			  }
			  newNode->next=temp->next;
			  temp->next=newNode;
		  }

		
	  }

	  return 0;
}

void deleteFirst(){

	  if( head == NULL){

		  printf("No node to delete\n");
	  }

	  else{

		  WK *temp = head;
		  head=head->next;
		  free(temp);
	  }
}

void deleteLast(){

	WK *temp =head;

	while( temp->next->next != NULL){

		temp=temp->next;
	}
	free(temp->next);
	temp->next=NULL;
}

int deleteAtpos( int pos){

	int count = countNode();

	if( pos <= 0 || pos<count){

	       printf("Invalid node position\n");
	       return -1;

	}
        else{

		if( pos == 1){

			deleteFirst();
		}
	        else if(pos == count+1){
            
			deleteLast();
		}

		else{

			WK *temp=head;
			

			while( pos-2){

				temp=temp->next;
				pos--;
			}
                        Node *temp2=temp->next;
			temp->next=temp->next->next;
			free(temp2);
			
		
		}
	}
	return 0;
}
	





void main(){

	 char choice;

	 do{
                

                 printf("****Node operations***\n");
		 printf("1.addNode\n");
		 printf("2.addFirst\n");
		 printf("3.addAtpos\n");
		 printf("4.deleteFirst\n");
		 printf("5.deleteLast\n");
		 printf("6.deleteAtpos\n");
		 printf("7.countNode\n");
		 printf("8.printNode\n");
		 printf("Enter choice\n");
		 int ch;
		 scanf("%d",&ch);


		 switch(ch) {

			    
			      case 1:
				      addNode();
				      break;
				
			      case 2:
			              
			              addFirst();
			              break;
			      case 3:
			           {
			              
			            int pos;
		                    printf("\nEnter position\n");
		                    scanf("%d",&pos);
		                    addAtpos(pos);
		                }
	                         break;
                              case 4:
                                      deleteFirst();
                                      break;

                              case 5:

                                      deleteLast();
                                      break;
                              case 6:
                                   {

                                    int pos;
                                    printf("\nEnter position\n");
                                    scanf("%d",&pos);
                                    deleteAtpos(pos);
                                }
                                 break;	
	                      case 7 :
	                             
			                 countNode();
		                         break;	   
			      case 8:	     	 
	                                 
					 printNode();
					 break;

			      default:
			            
			            printf("Wrong choice\n");
		                    break;
		 }
                  getchar();
                  printf("\nDo you want to continue ?\n");
                  scanf("%c",&choice);
                  }while( choice == 'Y'	|| choice == 'y');
}	 
		                    			     

